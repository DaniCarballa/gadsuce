%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Pruebas EUnit en el módulo formato
%%% @end
%%%-------------------------------------------------------------------
-module(formato_tests).

-include_lib("eunit/include/eunit.hrl").

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerPropiedadesAux.
%%      Se eliminan las propiedades repetidas
%% @end
%% --------------------------------------------------------------------
leer_propiedades_aux() ->
   ?assertEqual([prop1,prop2,prop3],
   formato:leer_propiedades_aux([prop1,prop2,prop3,prop1,prop2,prop3],[])).
              
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerPropiedades
%%      Se recogen en una lista, todas las propiedades
%% @end
%% --------------------------------------------------------------------
leer_propiedades() ->
   ?assertEqual([prop1,prop2,prop3,prop1,prop2,prop3],
    formato:leer_propiedades([
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3},
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3}],[])
   ).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerParticipantesAux
%%      Se eliminan los participantes repetidos
%% @end
%% --------------------------------------------------------------------
leer_participantes_aux() ->
   ?assertEqual([part1,part2,part3],
   formato:leer_participantes_aux([part1,part2,part3,part1,part2,part3],[])).
              
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerParticipantes
%       Se recogen en una lista, todos los participantes
%% @end
%% --------------------------------------------------------------------
leer_participantes() ->
   ?assertEqual([part1,part2,part3,part1,part2,part3],
    formato:leer_participantes([
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3},
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3}],[])
   ).  

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerActoresAux
%%      Se eliminan los actores repetidos
%% @end
%% --------------------------------------------------------------------
leer_actores_aux() ->
   ?assertEqual([actor1,actor2,actor3],
   formato:leer_actores_aux([actor1,actor2,actor3,actor1,actor2,actor3],[])).
              
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerActores
%%      Se recogen en una lista, todos los actores
%% @end
%% --------------------------------------------------------------------
leer_actores() ->
   ?assertEqual([actor1,actor2,actor3,actor1,actor2,actor3],
    formato:leer_actores([
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3},
                 {actor1,part1,llamada1,envi1,devuel1,prop1},
                 {actor2,part2,llamada2,envi2,devuel2,prop2},
                 {actor3,part3,llamada3,envi3,devuel3,prop3}],[])
   ).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función buscarRegistro
%%      Se busca 'register' para saber el nombre del nuevo proceso y
%%      así crear un nuevo participante desde 'spawn'
%% @end
%% --------------------------------------------------------------------
buscar_registro() ->
   ?assertEqual([{reg_eqc,a,creates,a,ok,propiedad1}],   
   formato:buscar_registro(
   [{actor1,erlang,register,[a,{var,1}],{var,2},propiedad1}],[],
    {actor1,reg_eqc,spawn,[],{var,1},propiedad1})).
    
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función leerSpawn
%%      Se busca 'spawn' y se crea desde ahí el nuevo participante
%% @end
%% --------------------------------------------------------------------
leer_spawn() ->
   ?assertEqual([
         {actor1,reg_eqc,spawn,[],{var,1},propiedad1},
         {reg_eqc,a,creates,a,ok,propiedad1},
         {actor1,erlang,register,[a,{var,1}],{var,2},propiedad1}],   
   formato:leer_spawn(
   [{actor1,reg_eqc,spawn,[],{var,1},propiedad1},
    {actor1,erlang,register,[a,{var,1}],{var,2},propiedad1}],[])).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función modificarValor
%%      Se modifica el valor devuelto por la llamada que produce el 
%%      contra-ejemplo.
%% @end
%% --------------------------------------------------------------------   
modificar_valor() ->
   ?assertEqual(
      [{actor1,reg_eqc,spawn,[],{var,1},propiedad1},
       {actor1,erlang,register,[a,{var,1}],{var,2},propiedad1},
       {actor1,erlang,register,[a,{var,1}],{error,badarg},propiedad1}],   
   formato:modificar_valor(
   [{actor1,reg_eqc,spawn,[],{var,1},propiedad1},
    {actor1,erlang,register,[a,{var,1}],{var,2},propiedad1},
    {actor1,erlang,register,[a,{var,1}],{var,2},propiedad1}],[])).

%%--------------------------------------------------------------------
%% @doc Caso de prueba donde se ejecutan todas las opciones
%% @end
%% --------------------------------------------------------------------
formato_test_() ->
   [{"Prop leer_propiedade_aux",    fun leer_propiedades_aux/0},
    {"Prop leer_propiedades",      fun leer_propiedades/0},
    {"Prop leer_participantes_aux", fun leer_participantes_aux/0},
    {"Prop leer_Participantes",    fun leer_participantes/0},
    {"Prop leer_actores_aux",       fun leer_actores_aux/0},
    {"Prop leer_actores",          fun leer_actores/0},
    {"Prop buscar_registro",       fun buscar_registro/0},
    {"Prop leer_spawn",            fun leer_spawn/0},
    {"Prop modificar_valor",       fun modificar_valor/0}
   ].
