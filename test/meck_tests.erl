%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Pruebas EUnit con Mocks para las funciones contraejemplo/0 
%%%      del módulo Read y generar_contraejemplo/1 del módulo Formato 
%%% @end
%%%-------------------------------------------------------------------

-module(meck_tests).

-include_lib("eunit/include/eunit.hrl").
-include("../include/gadsuce.hrl").
-define(F,"prueba_meck").

%%%%% FUNCIONES AUXILIARES %%%%%
leer_fichero2(N) ->
    F2 = ?OUTPUT_DIR ++ ?F, 
    case file:open(F2, [read]) of
        {ok, Fd} ->           
            L = leer_linea(Fd,[],N),
            file:close(Fd),
            L;
        {error, Motivo} ->
            {error, Motivo}
    end.
    
leer_linea(Fd,L2,N) ->
    case io:get_line(Fd, '\n') of
        eof -> L2;
        {error, Motivo} -> 
            {error,Motivo};
        Texto -> case N of
            1 -> %con \n            
            leer_linea(Fd,L2++[
               string:sub_string(Texto,1,string:len(Texto)-1)],N);
            2 -> %sin \n
            leer_linea(Fd,L2++[
               string:sub_string(Texto,1,string:len(Texto))],N)
            end
    end.
    
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función contraejemplo
%%      del módulo READ
%% @end
%% --------------------------------------------------------------------
contraejemplo() ->
   meck:new(eqc,[non_strict]),
   meck:expect(eqc,counterexamples,fun() -> 
   [{prop_registration,[[{set,{var,1},{call,reg_eqc,spawn,[]}},
                      {set,{var,2},{call,erlang,register,[a,{var,1}]}},
                      {set,{var,3},{call,erlang,register,[a,{var,1}]}}]]}]
                      end),
   ?assertEqual([
   {user,reg_eqc,spawn,   [],         {var,1},prop_registration},
   {user,erlang, register,[a,{var,1}],{var,2},prop_registration},
    {user,erlang, register,[a,{var,1}],{var,3},prop_registration}],
    read:contraejemplo()),
    meck:unload(eqc).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función generar_contraejemplo
%%      del módulo FORMATO
%% @end
%% --------------------------------------------------------------------
generar_contraejemplo() ->
   meck:new(eqc,[non_strict]),
   meck:expect(eqc,counterexamples,fun() -> 
   [{prop_registration,[[{set,{var,1},
                    {call,erlang,unregister,[a]}}]]}] end),
   formato:generar_contraejemplo(?F),
   ?assertEqual(["@startuml","autonumber 1","title prop_registration",
     "actor user","participant erlang",
      "user -> erlang : unregister([a])","activate erlang",
      "erlang --> user : {error,badarg}","deactivate erlang","@enduml"],
    leer_fichero2(1)),
    file:delete(?OUTPUT_DIR ++ ?F),
    meck:unload(eqc).

meck_test_() ->
   [
    {"Prop contraejemplo", fun contraejemplo/0},
    {"prop generar_contraejemplo", fun generar_contraejemplo/0}
   ].
