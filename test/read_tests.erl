%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Pruebas EUnit en el módulo read
%%% @end
%%%-------------------------------------------------------------------
-module(read_tests).

-include_lib("eunit/include/eunit.hrl").

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función interpretarProceso
%% @end
%% --------------------------------------------------------------------
interpretar_procesos() ->
   ?assertEqual({user,reg_eqc,spawn,[],{var,1},prop_registration},
      read:interpretar_procesos(
          {set,{var,1},{call,reg_eqc,spawn,[]}},prop_registration)
   ).
              
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función listaSimple
%% @end
%% --------------------------------------------------------------------
lista_simple() ->
   ?assertEqual([{user,reg_eqc,spawn,[],{var,1},nothing},
                 {user,erlang,register,[a,{var,1}],{var,2},nothing}],
   read:lista_simple([{set,{var,1},{call,reg_eqc,spawn,[]}},
                     {set,{var,2},{call,erlang,register,[a,{var,1}]}}]
                     ,[],nothing)
   ).                

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función listaCompuesta
%% @end
%% --------------------------------------------------------------------
lista_compuesta() ->
   ?assertEqual([
   {user,reg_eqc,spawn,   [],         {var,1},prop_registration},
   {user,erlang, register,[a,{var,1}],{var,2},prop_registration}],
   read:lista_compuesta([
   {prop_registration,
         [[{set,{var,1},{call,reg_eqc,spawn,  []}},
           {set,{var,2},{call,erlang,register,[a,{var,1}]}}]]}]
                     ,[])
   ). 


%%--------------------------------------------------------------------
%% @doc Caso de prueba donde se ejecutan todoas las opciones

%% @end
%% --------------------------------------------------------------------
read_test_() ->
   [{"Return process", fun interpretar_procesos/0},
    {"Return lista simple", fun lista_simple/0},
    {"Return lista compuesta", fun lista_compuesta/0}
   ].
