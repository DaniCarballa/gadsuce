%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Pruebas EUnit en el módulo writeText
%%% @end
%%%-------------------------------------------------------------------
-module(write_text_tests).

-include_lib("eunit/include/eunit.hrl").
-include("../include/gadsuce.hrl").
-define(F,"textCompTest").

%%--------------- Funciones auxiliares--------------------------------
leer_fichero(N) ->     
    case file:open(?F, [read]) of
        {ok, Fd} ->           
            L = leer_linea(Fd,[],N),
            file:close(Fd),
            L;
        {error, Motivo} ->
            {error, Motivo}
    end.

leer_fichero2(N) ->
    F2 = ?OUTPUT_DIR ++ ?F, 
    case file:open(F2, [read]) of
        {ok, Fd} ->           
            L = leer_linea(Fd,[],N),
            file:close(Fd),
            L;
        {error, Motivo} ->
            {error, Motivo}
    end.
    
leer_linea(Fd,L2,N) ->
    case io:get_line(Fd, '\n') of
        eof -> L2;
        {error, Motivo} -> 
            {error,Motivo};
        Texto -> case N of
            1 -> %con \n            
            leer_linea(Fd,L2++[
               string:sub_string(Texto,1,string:len(Texto)-1)],N);
            2 -> %sin \n
            leer_linea(Fd,L2++[
               string:sub_string(Texto,1,string:len(Texto))],N)
            end
    end.

escribir_fichero(Op) -> case file:open(?F, [write]) of
      {ok, Fd} -> case Op of
                  1 -> write_text:inicio(Fd);
                  2 -> write_text:opciones(Fd,titulo);
                  3 -> write_text:fin(Fd);
                  4 -> write_text:actores(Fd,[actor1,actor2,actor3]);
                  5 -> write_text:participantes(Fd,[part1,part2,part3]);
                  6 -> write_text:flecha_ida(Fd,actor1,part1);
                  7 -> write_text:flecha_vuelta(Fd,actor1,part1);
                  8 -> write_text:conversion_aux(Fd,
                        [{actor1,reg_eqc,spawn,[],r,prop1},
                         {reg_eqc,a,creates,a,ok,prop1},
                         {actor1,part2,llamada2,[],a,prop1}]
                        ,[prop1]);
                  9 -> write_text:conversion(Fd,{[user],[editor],
                  [{user,editor,edit,[1," "],{var,1},prop_editor},
                   {user,editor,edit,[3," "],{error,badarg},prop_editor2}],
                  [prop_editor,prop_editor2]
                  });
                  10 -> write_text:activar(Fd,user);
                  11 -> write_text:desactivar(Fd,user);
                  12 -> write_text:imprimir_creates(Fd,
                  [{reg_eqc,a,creates,a,ok,prop}])
                  end,
            file:close(Fd),              
            ok
   end.

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función inicio
%% @end
%% --------------------------------------------------------------------
inicio() ->  
   escribir_fichero(1),   
   ?assertEqual(["@startuml"],leer_fichero(1)),
   file:delete(?F).
   
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función opciones
%% @end
%% --------------------------------------------------------------------
opciones() ->  
   escribir_fichero(2), 
   ?assertEqual(["autonumber 1","title titulo"],leer_fichero(1)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función activar.
%% @end
%% --------------------------------------------------------------------
activar() ->  
   escribir_fichero(10),   
   ?assertEqual(["activate user"],leer_fichero(1)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función desactivar.
%% @end
%% --------------------------------------------------------------------
desactivar() ->  
   escribir_fichero(11),   
   ?assertEqual(["deactivate user"],leer_fichero(1)),
   file:delete(?F).


%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función fin.
%% @end
%% --------------------------------------------------------------------
fin() ->  
   escribir_fichero(3),   
   ?assertEqual(["@enduml"],leer_fichero(1)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función actores.
%% @end
%% --------------------------------------------------------------------
actores() ->  
   escribir_fichero(4),   
   ?assertEqual(["actor actor1",
                 "actor actor2",
                 "actor actor3"],leer_fichero(1)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función participantes.
%% @end
%% --------------------------------------------------------------------
participantes() ->  
   escribir_fichero(5),   
   ?assertEqual(["participant part1",
                 "participant part2",
                 "participant part3"],leer_fichero(1)),
   file:delete(?F).
   
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función flecha_ida.
%% @end
%% --------------------------------------------------------------------
flecha_ida() ->  
   escribir_fichero(6),   
   ?assertEqual(["actor1 -> part1"],leer_fichero(2)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función flecha_vuelta.
%% @end
%% --------------------------------------------------------------------
flecha_vuelta() ->  
   escribir_fichero(7),   
    ?assertEqual(["part1 --> actor1"],leer_fichero(2)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función imprimir_creates.
%% @end
%% --------------------------------------------------------------------
imprimir_creates() ->
   escribir_fichero(12),
   ?assertEqual(["create a","reg_eqc -> a : creates(a)",
   "activate a","a --> reg_eqc : ok","deactivate a"],leer_fichero(1)),
   file:delete(?F).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función conversion_aux.
%% @end
%% --------------------------------------------------------------------
conversion_aux() ->  
   escribir_fichero(8),   
    ?assertEqual(["actor1 -> reg_eqc : spawn([])",
                  "activate reg_eqc",
                  "create a",
                  "reg_eqc -> a : creates(a)",
                  "activate a",
                  "a --> reg_eqc : ok",
                  "deactivate a",
                  "reg_eqc --> actor1 : r",
                  "deactivate reg_eqc",
                  "actor1 -> part2 : llamada2([])",
                  "activate part2",
                  "part2 --> actor1 : a",
                  "deactivate part2"],leer_fichero(1)),
   file:delete(?F).
   
%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función conversion.
%% @end
%% --------------------------------------------------------------------
conversion() ->  
   escribir_fichero(9),
      ?assertEqual(["@startuml","autonumber 1","title prop_editor",
      "actor user","participant editor","user -> editor : edit([1,\" \"])",
      "activate editor","editor --> user : {var,1}","deactivate editor",
      "newpage prop_editor2","autonumber 1",
      "user -> editor : edit([3,\" \"])","activate editor",      
      "editor --> user : {error,badarg}","deactivate editor",
      "@enduml"],leer_fichero(1)),
      file:delete(?F).


%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función modificar nombre.
%% @end
%% --------------------------------------------------------------------   
modificar_nombre() ->
   ?assertEqual("fich_1",write_text:modificar_nombre("fich")).

%%--------------------------------------------------------------------
%% @doc Definición de caso de prueba de la función write file.
%% @end
%% --------------------------------------------------------------------   
write_file() ->     
   write_text:write_file({[user],[editor],
                  [{user,editor,edit,[1," "],{var,1},prop_editor},
                   {user,editor,edit,[3," "],{error,badarg},prop_editor2}],
                  [prop_editor,prop_editor2]},?F),
      ?assertEqual(["@startuml","autonumber 1","title prop_editor",
      "actor user","participant editor","user -> editor : edit([1,\" \"])",
      "activate editor","editor --> user : {var,1}","deactivate editor",
      "newpage prop_editor2","autonumber 1",
      "user -> editor : edit([3,\" \"])","activate editor",
      "editor --> user : {error,badarg}","deactivate editor",
      "@enduml"],leer_fichero2(1)),
      file:delete(?OUTPUT_DIR ++ ?F).
%%--------------------------------------------------------------------
%% @doc Caso de prueba donde se ejecutan todoas las opciones
%% @end
%% --------------------------------------------------------------------
write_text_test_() ->
   [{"Prop inicio",          fun inicio/0},
    {"Prop opciones",        fun opciones/0},
    {"Prop fin",             fun fin/0},
    {"Prop activar",         fun activar/0},
    {"Prop desactivar",      fun desactivar/0},
    {"Prop actores",         fun actores/0},
    {"Prop participantes",   fun participantes/0},
    {"Prop flechaIda",       fun flecha_ida/0},
    {"Prop flechaVuelta",    fun flecha_vuelta/0},
    {"Prop imprimir_creates",fun imprimir_creates/0},
    {"Prop conversionAux",   fun conversion_aux/0},
    {"Prop conversion",      fun conversion/0},
    {"Prop modificar_nombre",fun modificar_nombre/0},
    {"Prop write_file",      fun write_file/0}
   ].
