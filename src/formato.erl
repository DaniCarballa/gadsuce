%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Este módulo será el módulo principal donde se llamarán a los
%%%      otros dos módulos para el buen funcionamiento de la aplicación
%%% @end
%%%-------------------------------------------------------------------

-module(formato).

%% API pública
-export([leer_contraejemplo/1,leer_contraejemplo/0]).

%% API pública necesaria para realizar las pruebas de unidad
-export([generar_contraejemplo/1]).
-export([leer_propiedades_aux/2,leer_propiedades/2]).
-export([leer_participantes_aux/2,leer_participantes/2]).
-export([leer_actores_aux/2,leer_actores/2]).
-export([buscar_registro/3,leer_spawn/2,modificar_valor/2]).

-include("../include/gadsuce.hrl").

%%--------------------------------------------------------------------
%% @doc Se lee una lista de propiedades y se crea una lista que
%%      devuelve las propiedades sin repetir.Como por ejemplo :
%%      [prop1,prop1,prop2,prop2,prop3] => [prop1,prop2,prop3]
%% @spec leer_propiedades_aux(L::list, L2::list) -> list
%% @end
%%--------------------------------------------------------------------   
leer_propiedades_aux([],L) -> L;
leer_propiedades_aux([H|T],L)-> case lists:member(H,L) of
            true  -> leer_propiedades_aux(T,L);
            false -> leer_propiedades_aux(T,L++[H])
            end. 

%%--------------------------------------------------------------------
%% @doc Función que recoge las propiedades de una lista de tuplas
%% @spec leer_propiedades(L::list ,L2::list) -> list
%% @end
%%--------------------------------------------------------------------
leer_propiedades([],L) -> L;
leer_propiedades([H|T],L) ->
   leer_propiedades(T,L++[element(6,H)]).           
     
   
%%--------------------------------------------------------------------
%% @doc Se lee una lista de participantes y se crea una lista que
%%      devuelve los participantes sin repetir.Como por ejemplo :
%%      [part1,part1,part2,part2,part3] => [part1,part2,part3]  
%% @spec leer_participantes_aux(L::list, L2::list) -> list    
%% @end
%%--------------------------------------------------------------------   
leer_participantes_aux([],L) -> L;
leer_participantes_aux([H|T],L) ->
   case lists:member(H,L) of
            true  -> leer_participantes_aux(T,L);
            false -> leer_participantes_aux(T,L++[H])
            
   end.
   
%%--------------------------------------------------------------------
%% @doc Función que recoge los particiantes de una lista de tuplas
%%      Además los participantes pueden estar en la primera o en la 
%%      segunda posición, por ello es algo diferente a la función de
%%      leerActores ***
%% @spec leer_participantes(L::list, L2::list) -> list
%% @end
%%--------------------------------------------------------------------
leer_participantes([],L) -> L;
leer_participantes([H|T],L)-> case lists:member(H,L) of
            true  -> leer_participantes(T,L);
            false -> leer_participantes(T,L++[element(2,H)])%***
   end.

%%--------------------------------------------------------------------
%% @doc Se lee una lista de actores y se crea una lista que
%%      devuelve los actores sin repetir.Como por ejemplo :
%%      [actor1,actor1,actor2,actor2,actor3] => [actor1,actor2,actor3]
%% @spec leer_actores_aux(L::list, L2::list) -> list
%% @end
%%--------------------------------------------------------------------   
leer_actores_aux([],L) -> L;
leer_actores_aux([H|T],L) -> case lists:member(H,L) of
            true  -> leer_actores_aux(T,L);
            false -> leer_actores_aux(T,L++[H])
            end.

%%--------------------------------------------------------------------
%% @doc Función que recoge los actores de una lista de tuplas
%% @spec leer_actores(L::list, L2::list) -> list
%% @end
%%-------------------------------------------------------------------- 
leer_actores([],L) -> L;
leer_actores([H|T],L)-> leer_actores(T,L++[element(1,H)]).

%%--------------------------------------------------------------------
%% @doc Función que busca el registro donde se va registrar el 'Spawn' invocado
%%      anteriormente y recoge el nombre del participante para indicar que se
%%      va a crear y se añade a la lista con todos sus campos cubiertos.
%%      La tupla es de la forma : {actor1,reg_eqc,spawn,[],{var,1},propiedad1}
%%
%% @spec buscar_registro(L::list, L::list, E::tupla) -> list
%% @end
%%--------------------------------------------------------------------  
buscar_registro([],L,_E) -> L;
buscar_registro([H|T],L,E)-> 
         case element(3,H) == register of
            true -> case lists:member(element(5,E),element(4,H)) of
               true -> P = lists:nth(1,element(4,H)),
                         L++[{element(2,E),P,creates,P,ok,element(6,E)}];
               false -> buscar_registro(T,L,E)
               end;
            false -> buscar_registro(T,L,E)         
         end.

%%--------------------------------------------------------------------
%% @doc Función que busca el 'Spawn' para luego buscar el registro hacia
%%      donde va apuntar ese 'Spawn' 
%% @spec leer_spawn(L::list, L2::list) -> list
%% @end
%%--------------------------------------------------------------------  
leer_spawn([],L) -> L;
leer_spawn([H|T],L) ->
            case element(3,H) of
               spawn -> leer_spawn(T,L++[H]++buscar_registro(T,[],H));
               _ -> leer_spawn(T,L++[H])
            end.

%%--------------------------------------------------------------------
%% @doc Función para modificar el valor que devuelve la llamada donde
%%      se produce el contra-ejemplo
%% @spec modificar_valor(L::list, L2::list) -> list
%% @end
%%--------------------------------------------------------------------  
modificar_valor([],L) -> L;
modificar_valor([H|T],L) -> case T of
      [] -> modificar_valor(T,L++[
                        {element(1,H),element(2,H),
                         element(3,H),element(4,H),
                        {error,badarg},element(6,H)}]);
      _ -> modificar_valor(T,L++[H])
      end.

%%--------------------------------------------------------------------
%% @doc Función que recoge el nombre del fichero por defecto
%% @spec leer_contraejemplo() -> ok
%% @end
%%--------------------------------------------------------------------      
leer_contraejemplo() -> generar_contraejemplo(?OUTPUT_FILE).

%%--------------------------------------------------------------------
%% @doc Función que recoge el nombre del fichero introducido por el 
%%      usuario, tiene que ir entre comillas dobles : "fich1"
%% @spec leer_contraejemplo(F::string) -> ok
%% @end
%%--------------------------------------------------------------------  
leer_contraejemplo(F) -> generar_contraejemplo(F).

%%--------------------------------------------------------------------
%% @doc Leemos el contra-ejemplo del módulo 'read'. Si no devuelve el
%%      contra-ejemplo, se indica con un mensaje por pantalla en otro caso
%%      se crea una tupla de listas, en cuyo interior hay:
%%      - Lista de actores.
%%      - Lista de participantes.
%%      - Lista principal de la forma :
% [{actor, participante, llamada, valorEnviado, valorDevuelto, propiedadError}]
%%      - Lista de propiedades (para dividir el diagrama)
%%
%%      Esta tupla se pasa como parámetro a la función 'write_file' del 
%%      módulo 'write_text'.
%% @spec generar_contraejemplo(F::string) -> ok
%% @end
%%--------------------------------------------------------------------  
generar_contraejemplo(F) ->
      case C = read:contraejemplo() of
         false -> io:format("No counterexamples : Congratulations~n",[]);
         _-> L  = leer_actores_aux(leer_actores(C,[]),[]),
             L2 = leer_participantes_aux(leer_participantes(C,[]),[]), 
             L3 = modificar_valor(leer_spawn(C,[]),[]),
             L4 = leer_propiedades_aux(leer_propiedades(C,[]),[]),
             {ok, _Pid} = supervisor_format:start(), %Ejecutamos el supervisor
             write_text:write_file({L,L2,L3,L4},F)
      end.
         
         
         
         
         
         
         
         
