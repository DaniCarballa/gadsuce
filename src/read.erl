%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Este módulo se encargará de leer el contra-ejemplo para luego
%%%      pueda interpretarlo el formato propio.
%%% @end
%%%-------------------------------------------------------------------
-module(read).

%% API pública
-export([contraejemplo/0]).

%% API pública necesaria para realizar las pruebas de unidad
-export([lista_compuesta/2,interpretar_procesos/2,lista_simple/3]).
-include_lib("eqc/include/eqc.hrl").

%%--------------------------------------------------------------------
%% @doc Interpretación de cada proceso. La tupla que acepta como 
%%      parámetro es de la forma : {set,{var,1},{call,reg_eqc,spawn,[]}}.
%%      En la implementación de la función se puede ver los datos interesantes
%%      recogidos. Devuelve la tupla "Result" siguiente :
%%
% {actor, participante, llamada, valorEnviado, valorDevuelto, propiedadError}
%% @spec interpretar_procesos(E::tupla, P::atom()) -> Result 
%% @end
%%--------------------------------------------------------------------
interpretar_procesos(E,P) ->    
                           {user,                   %actor
                           element(2,element(3,E)), %participante
                           element(3,element(3,E)), %llamada
                           element(4,element(3,E)), %valor enviado
                           element(2,E),            %valor devuelto
                           P                        %propiedad error
                          }.
                        

%%--------------------------------------------------------------------
%% @doc Se crea una lista personalizada para mandarla al formato propio
%% @spec lista_simple(L::list,L2::list,P::atom()) -> list
%% @end
%%--------------------------------------------------------------------
lista_simple(L,L2,P) -> 
   case L of
      [] -> L2;   
      [H|T] -> case H of
               {set,{_,_},{call,_,_,_}} ->
                  lista_simple(T,L2++[interpretar_procesos(H,P)],P);
               _ -> lista_simple(T,L2,P)
               end
   end.

%%--------------------------------------------------------------------
%% @doc Se crea una lista personalizada para mandarla al formato propio.
%%      La diferencia entre la lista compuesta y la simple es que, la primera
%%      recoge el nombre de la propiedad donde se produjo el contraejemplo
%% @spec lista_compuesta(L :: list, L2 :: list) -> list
%% @end
%%--------------------------------------------------------------------
lista_compuesta(L,L2) ->
   case L of
      [] -> L2;
      [H|T] -> P = element(1,H),
               L3 = lista_simple(lists:flatten(element(2,H)),[],P),
               lista_compuesta(T,L2++L3)
    end.
   
   
%%--------------------------------------------------------------------
%% @doc Se recoge el contraejemplo.
%% @spec contraejemplo() -> undefined | list
%% @end
%%--------------------------------------------------------------------
contraejemplo() -> 
   case C = eqc:counterexamples() of
      undefined -> case D = eqc:counterexample() of
                     undefined -> false;
                     _-> lista_simple(lists:flatten(D),[],nothing)
                   end;
      _-> lista_compuesta(C,[])
   end.





