%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Este módulo se encargará de crear el texto compatible para
%%%      que el PlantUML lo entienda y la ejecución del mismo, para la
%%%      la creación de los diagramas de secuencia
%%% @end
%%%-------------------------------------------------------------------
-module(write_text).

%% API pública
-export([write_file/2]).

%% API pública necesaria para realizar las pruebas de unidad
-export([inicio/1,opciones/2,fin/1,activar/2,desactivar/2]).
-export([actores/2,participantes/2]).
-export([flecha_ida/3,flecha_vuelta/3]).
-export([conversion_aux/3,conversion/2]).
-export([modificar_nombre/1]).
-export([imprimir_creates/2]).

-include("../include/gadsuce.hrl").

%%--------------------------------------------------------------------
%% @doc Cabecera del texto compatible para PlantUML.
%% @spec inicio(Fd::file) -> ok
%% @end
%%--------------------------------------------------------------------
inicio(Fd) -> io:format(Fd,"~c~p~n",[$@,startuml]).

%%--------------------------------------------------------------------
%% @doc Opción para activar el autonumber para numerar las llamadas y
%%      se pone el nombre de la propiedad donde se produce el 
%%      contra-ejemplo a modo de título.
%% @spec opciones(Fd::file, T::string) -> ok
%% @end
%%--------------------------------------------------------------------
opciones(Fd,T) -> io:format(Fd,"~p ~p~n",[autonumber,1]),
                  io:format(Fd,"~p ~p~n",[title,T]).

%%--------------------------------------------------------------------
%% @doc Activa la ejecución.
%% @spec activar(Fd::file,P::atom()) -> ok
%% @end
%%--------------------------------------------------------------------
activar(Fd,P) -> io:format(Fd,"~p ~p~n",[activate,P]).

%%--------------------------------------------------------------------
%% @doc Desactiva la ejecución.
%% @spec desactivar(Fd::file,P::atom()) -> ok
%% @end
%%--------------------------------------------------------------------
desactivar(Fd,P) -> io:format(Fd,"~p ~p~n",[deactivate,P]).

%%--------------------------------------------------------------------
%% @doc Fin del texto compatible para PlantUML.
%% @spec fin(Fd::file) -> ok
%% @end
%%--------------------------------------------------------------------
fin(Fd)    -> io:format(Fd,"~c~p~n",[$@,enduml]).

%%--------------------------------------------------------------------
%% @doc Se leen los actores y se escriben en el texto compatible del
%%      PlantUML.
%% @spec actores(Fd::file, L::list) -> ok
%% @end
%%--------------------------------------------------------------------
actores(_Fd,[]) -> ok;
actores(Fd,[H|T])-> io:format(Fd,"~p ~p~n",[actor,H]),actores(Fd,T).

%%--------------------------------------------------------------------
%% @doc Se leen los participantes y se escriben en el texto compatible del
%%      PlantUML.
%% @spec participantes(Fd::file, L::list) -> ok
%% @end
%%--------------------------------------------------------------------
participantes(_Fd,[]) -> ok;
participantes(Fd,[H|T]) ->io:format(Fd,"~p ~p~n",[participant,H]),participantes(Fd,T).

%%--------------------------------------------------------------------
%% @doc Se escribe en el texto compatible la flecha de ida, añadiendo
%%      desde donde sale la llamada y hacia donde va.
%% @spec flecha_ida(Fd::file, Actor::string, Participante::string) -> ok
%% @end
%%--------------------------------------------------------------------
flecha_ida(Fd,Actor,Participante) ->
   io:format(Fd,"~p",[Actor]),        %Actor
   io:format(Fd," ~c~c ",[$-,$>]),    %Flecha Ida
   io:format(Fd,"~p",[Participante]). %Participante

%%--------------------------------------------------------------------
%% @doc Se escribe en el texto compatible la flecha de vuelta, añadiendo
%%      desde donde sale la llamada y hacia donde vuelve.
%% @spec flecha_vuelta(Fd::file, Actor::string, Participante::string) -> ok
%% @end
%%--------------------------------------------------------------------
flecha_vuelta(Fd,Actor,Participante) -> 
   io:format(Fd,"~p",[Participante]),   %Participante
   io:format(Fd," ~c~c~c ",[$-,$-,$>]), %Flecha vuelta
   io:format(Fd,"~p",[Actor]).          %Actor.

%%--------------------------------------------------------------------
%% @doc Se imprime creates por separado para tener activada toda la llamada
%%      de spawn.
%% @spec imprimir_creates(Fd::file, T::list) -> ok
%% @end
%%--------------------------------------------------------------------  
imprimir_creates(_Fd,[]) -> ok;
imprimir_creates(Fd,[H|_T]) -> 
   case element(3,H) == creates of
   true ->
      io:format(Fd,"~p ~p~n",[create,element(4,H)]),
      flecha_ida(Fd,element(1,H),element(2,H)), 
      
      io:format(Fd," ~c ",[$:]),           % :
      io:format(Fd,"~p",[element(3,H)]),   % llamada
      io:format(Fd,"~c",[$(]),             % (
      io:format(Fd,"~p",[element(4,H)]),   % valor enviado
      io:format(Fd,"~c~n",[$)]),           % )
      
      activar(Fd,element(2,H)),
           
      flecha_vuelta(Fd,element(1,H),element(2,H)), 

      io:format(Fd," ~c ",[$:]),           % :
      io:format(Fd,"~p~n",[element(5,H)]), % valor devuelto
      desactivar(Fd,element(2,H));
   false -> ok
   end.
    
%%--------------------------------------------------------------------
%% @doc Función donde se escriben la mayor parte del contenido del texto
%%      compatible para que el PlantUML pueda interpretarlo. La lista es 
%%      una lista de tuplas "L" es de la siguiente forma :
% [{actor, participante, llamada, valorEnviado, valorDevuelto, propiedadError}].
%% @spec conversion_aux(Fd::file, L::list, L2::list) -> ok 
%% @end
%%--------------------------------------------------------------------           
conversion_aux(_Fd,[],_L) -> ok;
conversion_aux(Fd,[H|T],L) ->
   
   %Si hay varias propiedades que fallan las ponemos en diferentes
   %diagramas
   case lists:member(element(6,H),L) of
      false -> io:format(Fd,"~p ~p~n",[newpage,element(6,H)]),
               io:format(Fd,"~p ~p~n",[autonumber,1]);
      true -> ok               
   end,
     
   case element(3,H) /= creates of
   true ->
      flecha_ida(Fd,element(1,H),element(2,H)), 
      
      io:format(Fd," ~c ",[$:]),           % :
      io:format(Fd,"~p",[element(3,H)]),   % llamada
      io:format(Fd,"~c",[$(]),             % (
      io:format(Fd,"~p",[element(4,H)]),   % valor enviado
      io:format(Fd,"~c~n",[$)]),           % )
      
      activar(Fd,element(2,H)),
      
      case element(3,H) == spawn of
         true -> imprimir_creates(Fd,T);
         false -> ok
      end,
      
      flecha_vuelta(Fd,element(1,H),element(2,H)), 

      io:format(Fd," ~c ",[$:]),           % :
      io:format(Fd,"~p~n",[element(5,H)]), % valor devuelto
      desactivar(Fd,element(2,H));
      
   false -> ok
   end,
   %Si la propiedad no esta en la lista de propiedades usadas
   case  lists:member(element(6,H),L) of
      false -> conversion_aux(Fd,T,L++[element(6,H)]);
      true  -> conversion_aux(Fd,T,L)
   end.

%%--------------------------------------------------------------------
%% @doc Función principal donde se crea toda la estructura del fichero
%%      para que PlantUML pueda entenderlo. La tupla es de la forma :
%%      {[user],[editor],
%%       [{user,editor,edit,[1," "],var,1},prop_editor}],
%%       [prop_editor,prop_editor2]}
%%
%% @spec conversion(Fd::file, T::tupla) -> ok
%% @end
%%--------------------------------------------------------------------   
conversion(Fd,T) -> 
      inicio(Fd),  
      Titulo = lists:nth(1,element(4,T)),
      opciones(Fd,Titulo),  
      actores(Fd,element(1,T)),
      participantes(Fd,element(2,T)),
      conversion_aux(Fd,element(3,T),[Titulo]),
      fin(Fd).

%%--------------------------------------------------------------------
%% @doc Función para modificar el nombre y que nunca se repita el mismo
%% @spec modificar_nombre(Fich::string) -> ok
%% @end
%%--------------------------------------------------------------------
modificar_nombre(Fich) ->
   %Ori = nombre fichero original donde buscamos el "_"
   Ori = string:sub_string(Fich,1,string:rstr(Fich,"_")),
   Pos = string:rchr(Fich,$_),
   case Pos of
      0 -> Fich ++ "_1";
      _ -> Num = string:sub_string(Fich,Pos+1,string:len(Fich)),
           {NumN,_L} = string:to_integer(Num),
            Ori ++ integer_to_list(NumN + 1)
   end.
   
%%%%%%%%%%%%%%%%%%%%%%%% FUNCIONES PÚBLICAS %%%%%%%%%%%%%%%%%%%%%%%%%%

%%--------------------------------------------------------------------
%% @doc Función que escribe un fichero con texto compatible para el 
%%      PlantUML. Recibe como parámetro una tupla de listas T = {L,L2,L3,L4}
%%      las cuales equivaldrían a:
%%      - L  = Lista de actores
%%      - L2 = Lista de participantes
%%      - L3 = Lista principal de la forma :
% [{actor, participante, llamada, valorEnviado, valorDevuelto, propiedadError}]
%%      - L4 = Lista de propiedades (para dividir el diagrama) donde
%%        ocurrieron los contra-ejemplos.
%%
%% @spec write_file(T::list,F1::string) -> ok | {error, Motivo}
%% @end
%%--------------------------------------------------------------------
write_file (T,F1) ->

   F = ?OUTPUT_DIR ++ F1,
   
   case filelib:is_file(F) of
      true -> write_file(T,modificar_nombre(F1));
      false ->        
         case file:open(F, [write]) of
            {ok, Fd} -> 
                  conversion(Fd,T),                    
                  ok = file:close(Fd),
                  %Ejecutamos la función para que cree el diagrama
                  runner:exe(F),              
                  ok;         
            {error, Motivo} ->
               {error, Motivo}
         end
   end. 
   


