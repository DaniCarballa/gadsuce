%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Módulo para la ejecución del PlantUML.
%%% @end
%%%-------------------------------------------------------------------
-module(runner).
-behaviour(gen_server).

-include("../include/gadsuce.hrl").

%% Public API
-export([iniciar/1,exe/1]).

%% Internal API (gen_server)
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

%% Macro definition
-define(CHECKER, runner).

%%--------------------------------------------------------------------
%% @doc Iniciar el comprobador.
%% @spec iniciar(P::atom()) -> ok
%% @end
%% -------------------------------------------------------------------
iniciar(P) ->
    gen_server:start_link({local, ?CHECKER}, ?MODULE, P, []).

%%--------------------------------------------------------------------
%% @doc Ejecutar el PlantUML para generar el diagrama
%% @spec exe(P1::string) -> ok
%% @end
%% -------------------------------------------------------------------
exe(P1) -> gen_server:cast(?CHECKER, {exe,P1}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%--------------------------------------------------------------------
%% @doc Función que es llamada por la función iniciar.
%% Result => {ok, {P, []}}
%% @spec init(P::atom()) -> Result
%% @end
%% -------------------------------------------------------------------
init(P) ->
    {ok, {P, []}}.

%%--------------------------------------------------------------------
%% @doc Manejador de la función "cast"
%% @spec handle_cast(T::tupla, D::atom()) -> {noreply, ok}
%% @end
%% -------------------------------------------------------------------   
handle_cast({exe,P1},_D) ->

    %Vemos si está el ejecutable en la carpeta ./lib     
    case filelib:is_file(?LIBRARY_DIR ++ ?PLANTUML_JAR) of
    true -> _CmdOutput = os:cmd("java -jar " ++ ?LIBRARY_DIR ++ ?PLANTUML_JAR ++ " " ++
     P1),
             io:format("->> Diagram created! ~n"),
             {noreply, ok};
    false -> io:format("->> ERROR : PlantUML not found, diagram is not created ~n"),
             {noreply, ok}
    end .

%%--------------------------------------------------------------------
%% @doc Manejador de la función "call"
%% @spec handle_call(Call::atom(), From::atom(), NombreEstado::atom()) -> {noreply, NombreEstado}
%% @end
%% -------------------------------------------------------------------  
handle_call(call, _From, NombreEstado) ->
    {noreply, NombreEstado}.

%%--------------------------------------------------------------------
%% @doc Manejador de la función "call"
%% @spec handle_info(Info::term(), NombreEstado::atom()) -> {noreply, NombreEstado}
%% @end
%% -------------------------------------------------------------------  
handle_info(_Info, NombreEstado) ->
    {noreply, NombreEstado}.

%%--------------------------------------------------------------------
%% @doc Manejador de la función "call"
%% @spec code_change(Old::term(), NombreEstado::atom(), D::atom()) ->  {ok, NombreEstado}
%% @end
%% -------------------------------------------------------------------  
code_change(_Old, NombreEstado, _) ->
    {ok, NombreEstado}.

%%--------------------------------------------------------------------
%% @doc Manejador de la función "terminate"
%% @spec terminate(Normal::atom(), Estado::atom()) -> ok
%% @end
%% -------------------------------------------------------------------  
terminate(normal, _Estado) ->
    ok.
