%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Daniel Carballa Besada
%%% @doc Supervisor para comprobar la correcta ejecución del proceso
%%%      que llamará al PlantUML
%%% @end
%%%-------------------------------------------------------------------
-module(supervisor_format).
-behaviour(supervisor).

%% API pública
-export([start/0]).

%% API interna
-export([init/1]).

%%--------------------------------------------------------------------
%% @doc Arrancar el servidor.
%% @spec start() -> ok
%% @end
%% -------------------------------------------------------------------
start() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%--------------------------------------------------------------------
%% @doc Función principal que configurará el proceso hijo (child)
%%      que se va tener.
%% @spec init(L::list) -> {ok, {SupFlags, ChildSpecs}}
%% @end
%% -------------------------------------------------------------------
init([]) ->

   %estrategia reinicio, 1 reinicio cada 10 segundos
   SupFlags = #{strategy => one_for_one, intensity => 1, period => 10},
   
   % Parámetros proceso hijo
   ChildSpecs = [#{ id => runner,
                    start => {runner, iniciar, [1]},
                    restart => permanent,
                    shutdown => 20,
                    type => worker,
                    modules => [runner]}],
    {ok, {SupFlags, ChildSpecs}}.
    
    
    
    
