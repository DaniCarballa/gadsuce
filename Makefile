SILENT=@
SRC_DIR=src
TEST_DIR=test
BIN_DIR=ebin
BIN_DIR_MECK=/home/dani/meck/ebin
SAMPLES_DIR=samples
ERLC=erlc
DOC_DIR=doc

all: compile edoc tests

compile:
	$(SILENT) echo "Initiating compilation"
	$(SILENT) mkdir -p $(BIN_DIR)
	$(ERLC) -o $(BIN_DIR) $(SRC_DIR)/*.erl
	$(ERLC) -o $(BIN_DIR) $(TEST_DIR)/*.erl

edoc:
	$(SILENT) echo "Generating EDOC"
	erl -noshell -run edoc_run application "'Gadsuce'" '"."' '[]'

dialyzer:
	$(SILENT) mkdir -p $(BIN_DIR)/debug
	erlc -o $(BIN_DIR)/debug +debug_info $(SRC_DIR)/*.erl
	dialyzer -Wunmatched_returns \
		 -Werror_handling    \
		 -Wrace_conditions   \
		 -Wunderspecs $(BIN_DIR)/debug/*.beam

tests:
	$(SILENT) echo "Executing tests (EUnit):"
	$(ERLC) -o $(BIN_DIR) $(SAMPLES_DIR)/*.erl
	erl -noshell -pa $(BIN_DIR) -s eunit test formato_tests -s init stop
	erl -noshell -pa $(BIN_DIR) -s eunit test read_tests -s init stop
	erl -noshell -pa $(BIN_DIR) -s eunit test write_text_tests -s init stop
	#erl -noshell -pa $(BIN_DIR) -pa $(BIN_DIR_MECK) -s eunit test meck_tests -s init stop

sample: compile 
	$(SILENT) rm -f .eqc-info current_counterexample.eqc
	$(ERLC) -o $(BIN_DIR) $(SAMPLES_DIR)/*.erl
	erl -pa $(BIN_DIR)

clean:
	$(SILENT) echo "Deleting files"
	$(SILENT) rm -rf $(BIN_DIR)
	$(SILENT) rm -f $(DOC_DIR)/*.html $(DOC_DIR)/*.css $(DOC_DIR)/edoc-info $(DOC_DIR)/erlang.png
	$(SILENT) rm -f .eqc-info current_counterexample.eqc

