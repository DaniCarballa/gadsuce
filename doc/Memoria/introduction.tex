\chapter{Introducción}
\minitoc
\label{chap:introduccion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Exponer de qué va este proyecto, sus líneas maestras, objetivos,   %
%           etc.                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine{E}{l} proyecto que en esta memoria se expone lleva por
título ``\textbf{Generación automática de diagramas de secuencia UML a
  partir de contra-ejemplos en pruebas de unidad}''.

En este capítulo se van a comentar los objetivos y la motivación que llevaron a la creación de este proyecto además de una breve descripción de las fases y pasos seguidos para su elaboración.

\section{Objetivo y motivación}

En un mundo donde el software se va arraigando a pasos agigantados en la vida cotidiana de las personas, es necesario seguir un proceso de desarrollo software de calidad. Esto quiere decir, que todo producto software, debe ser construido mediante un proceso de desarrollo que le infiera calidad en cada una de sus etapas, siendo las pruebas, algo indispensable en dicho desarrollo. Lamentablemente en muchos casos, las pruebas siguen relegándose aún al final de dicho proceso, con el consiguiente riesgo de que no se completen apropiadamente por falta de tiempo. \newpage

La calidad en el software puede definirse como \textit{``El grado en el que un conjunto de características inherentes cumple con los requisitos''}\footnote{UNE-EN ISO 9000 : 2005 ``Sistemas de gestión de calidad. Fundamentos y Vocabulario''.}. o también como el \textit{``Conjunto de propiedades y características de un producto o servicio que le confieren aptitud para satisfacer unas necesidades explícitas o implícitas''}\footnote{ISO 8402 que complementa a la ISO 9000.}.
Como podemos ver, no existe una definición única y va ser el cliente quien juzgue si el producto cumple con los requisitos demandados y en qué grado.

Con las pruebas software podemos, no obstante, contribuir a que esa valoración del cliente sea positiva \textit{revelando la presencia de errores} (cuando fallan) o \textit{mostrando que todos los intentos de hacer fallar al software con respecto a su especificación fracasaron} (cuando pasan). Dicho de otro modo, en términos de calidad, las pruebas exitosas \textit{son las que encuentran errores}.

Dentro de un proyecto software, la realización de las pruebas puede llevarse a cabo a diferentes \emph{niveles}: 
\begin{itemize}
	\item \textbf{Unidad}: Se valida cada módulo de forma independiente, asegurándose del correcto funcionamiento de cada función/método (dentro del módulo) por separado y en caso de necesitarse, juntos.
	\item \textbf{Integración}: Se valida la combinación o unión de varios módulos ya probados. En este nivel de pruebas, se prueba que varios módulos trabajen juntos de forma unificada.
	\item \textbf{Sistema}: Se valida el despliegue del software al completo en un entorno de producción.
	\item \textbf{Aceptación}: Se comprueba el grado de cumplimiento de los requisitos por parte de los usuarios o clientes finales. 
\end{itemize}

A cualquiera de estos niveles (exceptuando el de aceptación), la implementación de las pruebas puede desarrollarse usando métodos tradicionales, en los que se especifican cada uno de los casos de prueba manualmente, o usando métodos más modernos y avanzados, como por ejemplo desarrollarlas en base a propiedades.

Las pruebas basadas en propiedades (PBT, \emph{Property-Based Testing}) son una estrategia de pruebas que busca contrastar el cumplimiento de ciertas propiedades en el software. Para ello, se definen dichas propiedades a alto nivel, y con una herramienta se generan automáticamente casos de prueba concretos con el fin de validarlas. Se trata de una estrategia de pruebas muy efectiva y que permite generar y ejecutar gran cantidad de pruebas muy fácilmente.

Se podría considerar como un ejemplo práctico, la creación de un módulo de manipulación de listas que implemente las siguientes funciones:
\begin{itemize}
	\item  \texttt{filter/2}: dada una lista de enteros y un entero 'N', queremos que devuelva la lista que contiene todos los elementos iguales o menores a 'N'.
	\item \texttt{reverse/1}: dada una lista de enteros, queremos que devuelva la misma lista en orden inverso.
	\item \texttt{concatenate/1}: dada una lista de listas, queremos que devuelva una lista como resultado de unir todas las listas.
	\item \texttt{flatten/1}: dada una lista de listas anidadas, queremos que devuelva una lista después de aplanar las listas anidadas.
\end{itemize}
Empleando estrategias tradicionales, para la validación de dicho módulo habría que realizar pruebas individuales por cada función donde deberíamos elegir tanto los valores de entrada concretos como los resultados esperados (calculados por nosotros):

\begin{figure}[h]
	\centering
	\includegraphics[width=14cm]{imagenes/unit.PNG}
	\caption{Ejemplo de pruebas diseñadas manualmente.}
	\label{unit}
\end{figure}

Empleando una estrategia basada en PBT, escribiríamos propiedades genéricas para cada función, donde los valores de entrada se generan aleatoriamente utilizando una función que produce valores de un determinado tipo, y el resultado esperado es validado algorítmicamente o comparado con un oráculo en el que se confía:

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{imagenes/prop.PNG}
	\caption{Ejemplos de propiedades para pruebas usando PBT.}
	\label{prop}
\end{figure}

En este caso (y haciendo alusión a este ejemplo), se crean listas de enteros aleatorias (\verb+list(int())+) y se realizan muchas pruebas por cada propiedad siendo la herramienta PBT la encargada de generarlas y de chequearlas.


En el primer caso, son pruebas realizadas con EUnit~\cite{erlangeunit}, que es una herramienta de pruebas de la familia xUnit para Erlang, que es el lenguaje en el que se escribe el ejemplo. Se ejecutan con la instrucción:

\begin{center}
	\texttt{eunit:test(manipulating\_tests).}
\end{center}

\noindent y como se puede apreciar en la Figura \ref{eunit}, las 4 pruebas pasan satisfactoriamente.

\begin{figure}[h]
	\centering
	\includegraphics[width=8cm]{imagenes/ejeunit.PNG}
	\caption{Ejemplo ejecución con EUnit.}
	\label{eunit}
\end{figure}

En el segundo caso, son pruebas realizadas con QuickCheck~\cite{quviqindex}, herramienta PBT para Erlang, con la cual cada ejecución realiza 100 pruebas concretas por cada propiedad, pudiéndose aumentar o disminuir manualmente el número de estas pruebas. Hay varias formas de ejecutar estas pruebas. Con \texttt{eqc:module(módulo)} se ejecutan todas las pruebas de las propiedades presentes en \texttt{módulo} y con \texttt{eqc:quickcheck(modulo:propiedad())} se ejecutan pruebas para la propiedad concreta \texttt{propiedad()} definida en \texttt{módulo}. La Figura \ref{pbt} muestra un ejemplo de ejecución usando el primero de estos dos formatos:
\begin{center}
	\texttt{eqc:module(manipulating\_eqc).}
\end{center}
\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{imagenes/ejpbt.PNG}
	\caption{Ejemplos de ejecución para pruebas usando PBT.}
	\label{pbt}
\end{figure}

Para visualizar un ejemplo de las listas de enteros generadas durante la ejecución de las pruebas (o de cualquier otro tipo de dato que se use en la definición de propiedades), puede usarse la función \texttt{eqc\_gen:sample/1} (ver Figura \ref{gen}).

\begin{center}
	\texttt{eqc\_gen:sample(eqc\_gen:list(eqc\_gen:int())).}
\end{center}
\begin{figure}[h]
	\centering
	\includegraphics[width=9cm]{imagenes/gen.PNG}
	\caption{Listas de enteros generadas aleatoriamente.}
	\label{gen}
\end{figure}

Como es de fácil comprender, estrategias como PBT ayudan mucho a agilizar el \emph{testing} ya que se generan casos que un desarrollador quizá los pasaría por alto, y se comprueban muchos más escenarios de los que se podrían programar manualmente.

No obstante, cuando se genera un caso de prueba que falla y por tanto demuestra el incumplimiento de la propiedad en la versión actual del software, resulta de extremo interés examinar ese contra--ejemplo para poder corregir la situación. El formato en el que se presentan dichos contra-ejemplos depende, normalmente, de la herramienta de PBT utilizada, dificultando en cierta manera el trabajo, sobre todo, para personas sin perfil técnico.

Así, la Figura \ref{contraejemploQC}, muestra un contra--ejemplo generado por la herramienta PBT QuickCheck mucho más elaborado que los mostrados anteriormente. Como se puede apreciar, no es una lectura sencilla. \\

Por lo tanto, la idea principal de este proyecto es crear una herramienta que favorezca la visualización de contra-ejemplos en un formato estandarizado, esto es, que no dependa del formato generado tanto por QuickCheck (u otras herramientas basadas en PBT). En concreto, se perseguirá la visualización en UML, integrando nuestro sistema con la herramienta PlantUML, pero de modo que sea fácilmente sustituible por otras herramientas de generación de diagramas UML, y también que se pueda utilizar para representar contra--ejemplos de otras herramientas PBT. De esta forma, se favorecerá la mantenibilidad y la extensibilidad del sistema. \\

\begin{figure}[hp]
	\centering
	\includegraphics[width=\textwidth]{imagenes/contra_ejemplo1.PNG}
	\caption{Contra--ejemplo generado con QuickCheck.}
	\label{contraejemploQC}
\end{figure}
\clearpage
Se ha escogido UML~\cite{uml} como formato objetivo porque es un estándar ampliamente conocido y utilizado, y los diagramas de secuencia en concreto porque tienen los tres elementos necesarios para la representación de un contra--ejemplo:
\begin{itemize}
	\item \textbf{Participantes}: Componentes involucrados en la interacción que desencadena el contra--ejemplo, que serán representados por \emph{líneas de vida}, rectángulos acompañados de una línea vertical que representa la existencia de dichos participantes durante la ejecución del escenario de prueba.
	\item \textbf{Actividad}: Participación activa de los componentes en el escenario, representada por \emph{activaciones}, rectángulos más finos encima de la línea de vida que representan la actuación de alguna actividad por parte del participante correspondiente.
	\item \textbf{Mensajes}: Métodos/funciones invocados o mensajes enviados entre los participantes en el escenario, representados por flechas entre activaciones de sus líneas de vida. Dependiendo del tipo de mensaje enviado (invocación, retorno, etc.), se utiliza un tipo de flecha/línea u otro.
\end{itemize}	

\begin{figure}[h]
	\centering
	\includegraphics[width=13cm]{imagenes/Ejuml1.PNG}
	\caption{Elementos básicos de un diagrama de secuencia UML}
	\label{Ejuml1}
\end{figure}

\clearpage
Del mismo modo, se ha escogido PlantUML~\cite{plantuml} para la creación de los diagramas de secuencia UML porque es una herramienta basada en texto y es muy fácil de leer y escribir para personas sin perfil técnico ya que se encuentra \textit{``casi''} en lenguaje natural (véanse las Figuras \ref{Ejuml1} y \ref{EJplantuml1}). Además, se puede exportar a SVG o PNG y está en fase beta la generación de ficheros XML.

\begin{figure}[h]
	\centering
	\includegraphics[width=14cm]{imagenes/EJplantuml1.PNG}
	\caption{Elementos básicos de un diagrama de secuencias en PlantUML}
	\label{EJplantuml1}
\end{figure}


\section{Fases del trabajo}
\begin{itemize}
	\item \textbf{Lectura y estudio de la documentación de las herramientas QuickCheck y Plant\-UML.}\newline
En primer lugar, nos hemos familiarizado con: 
\begin{itemize}
	\item La herramienta de generación automática de pruebas llamada  QuickCheck y con la forma que tiene de devolver los contra--ejemplos para poder recolectar la información. Dado que se trata de una herramienta implementada en el lenguaje de programación funcional Erlang, se determinó que se utilizaría este lenguaje para la implementación de nuestra herramienta, por lo que también hemos tenido que repasar la sintaxis de Erlang y su librería OTP para adquirir la destreza necesaria a la hora de comenzar con el proyecto.
	\item La herramienta de creación de diagramas de secuencia PlantUML y la forma que tiene el texto compatible para que pueda procesarlo.
\end{itemize}
\item \textbf{Análisis y especificación de requisitos.}\newline
En este apartado, se hizo el análisis del sistema y el levantamiento de los requisitos, es decir, las funciones o actividades más características que puede realizar el usuario o actor. Al ser una herramienta de soporte, se puede decir que el caso de uso es único, ya que la función principal es transformar el contra--ejemplo en un diagrama UML.
\item \textbf{Diseño.}\newline
El diseño es una parte importante del proyecto, ya que supone concretar nuestra visión general de la herramienta, separando tareas y responsabilidades en diferentes módulos. Se utiliza una arquitectura general basada en el patrón \textit{``Pipe and Filter''}~\cite{pipeandfilter}.
\item \textbf{Implementación.}\newline
Definida la estructura general del proyecto, se procede con la implementación de las responsabilidades y tareas de cada módulo donde se aplicarán buenas prácticas de diseño, respectando convenciones de nombres, indentando el código, documentando, etc., además de usar patrones (\emph{behaviours}) de Erlang/OTP.
\item \textbf{Diseño y ejecución de las pruebas.}\newline
Se diseñan pruebas de unidad para comprobar que cada módulo por separado funciona correctamente y pruebas de integración para tener la certeza de que la herramienta funciona como debería.
\item \textbf{Preparación de la documentación y redacción de la memoria.}\newline
Finalmente, se redacta la presente memoria del proyecto explicando las decisiones de diseño establecidas y el detalle del resto de actividades anteriormente mencionadas.
\end{itemize}

Para la planificación y seguimiento de este proyecto se ha usado una metodología ágil con un seguimiento semanal donde se proponían nuevos enfoques y mejoras en el proyecto, como se detallará en el capítulo correspondiente.













