\chapter{Implementación}
\minitoc
\label{chap:implementacion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Exponer las partes relevantes de la implementación                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine{E}{n} este capítulo se hablará de la implementación de la herramienta
explicando la función de cada módulo, sus funciones internas y las decisiones llevadas a cabo, además de aplicar buenas prácticas de diseño para adaptarse al estándar y que sea entendible por otros desarrolladores.

\section{Estructura del proyecto}

La herramienta está dividida en cinco módulos bien diferenciados. Cada módulo tiene sus responsabilidades que son independientes de los demás módulos. En la Figura \ref{diagramaclases} se muestra un diagrama de clases con la representación de cada módulo y sus funciones.

\begin{figure}[h]
	\centering
	\includegraphics[width=12cm]{imagenes/Diagramaclases.pdf}
	\caption{Diagrama de clases}
	\label{diagramaclases}
\end{figure}

La estructura básica de un proyecto Erlang cuenta con los siguiente directorios:
\begin{itemize}
	\item \texttt{src}: Este directorio contendrá el código fuente. Todos los ficheros cuya extensión sea \textbf{\emph{.erl}}.
	\item \texttt{ebin}: En este directorio se almacenarán todos los ficheros con extensión \textbf{\emph{.beam}}, es decir, los archivos binarios (compilados) de la aplicación.
	\item \texttt{include}: En este directorio se almacenan los ficheros con extensión \textbf{\emph{.hrl}}, es decir, los ficheros de cabeceras.
	\item \texttt{test}: Donde se almacena los ficheros de pruebas de la aplicación.	
\end{itemize}

Además de los directorios comentados, tenemos algunos más en nuestro repositorio, como:
\begin{itemize}
	\item \texttt{doc}: Este directorio contendrá todos los ficheros generados por la herramienta de documentación \emph{EDoc}~\cite{erlangedoc}. Se corresponden con la documentación técnica de la aplicación.
	\item \texttt{lib}: En este directorio se almacenan binarios o ejecutables externos o de terceras partes, en nuestro caso \emph{plantuml.jar}.
	\item \texttt{samples}: Este directorio contendrá los ejemplos propuestos.         
\end{itemize}

También se incluyen un archivo \emph{Makefile} que ejecutará y automatizará el compilado y la creación de la documentación y un fichero \emph{README} donde se explican los pasos para compilar y ejecutar el proyecto.

Todos los módulos siguen la convención de nombres \emph{snake\_case} que se caracteriza por que los nombres empiezan con letra minúscula y los nombres compuestos llevan un guión bajo, que es la convención \emph{de facto} en Erlang.

\subsection{Módulo de lectura (\textit{Read})}

La función de este módulo es leer el contra--ejemplo producido por herramientas PBT, para luego recoger los datos de mayor interés y enviarlos al módulo \texttt{Formato} en una estructura entendible.

Esta herramienta está adaptada a QuickCheck, por lo tanto, si se quisiera usar otra herramienta PBT habría que implementar otro módulo o actualizar éste. Las funciones o responsabilidades son las siguientes:
\begin{itemize}
	\item \texttt{contraejemplo/0}: Es la función principal donde tenemos que recoger el contra--ejemplo producido por QuickCheck. El problema de esta función es que el desarrollador no tiene conocimiento alguno de cómo el usuario va a realizar las pruebas, si es usando \texttt{eqc:quickcheck} o \texttt{eqc:module}. Haciendo alusión a este problema comentado en la parte de \emph{Análisis y Diseño}, se ha implementado la función de tal manera que primero compruebe si se ejecutaron las pruebas con \texttt{eqc:module} y en caso negativo, si lo hicieron con \texttt{eqc:quickcheck}. De cualquier forma, las dos funciones devuelven un \texttt{undefined} (si no existe ningún contra--ejemplo) o una lista. Cada función será tratada de diferente forma.
	\item \texttt{lista\_simple/3}: En esta función, se lee toda la lista pasada como parámetro y se recogen los datos de mayor importancia, como los participantes, las llamadas, etc, para luego hacer una llamada a la función \texttt{interpretar\_pro\-ce\-sos/2} donde se guardarán los datos recogidos. Recibe tres parámetros en la cabecera: una lista, una lista vacía y el nombre de una propiedad (este último parámetro se explica mejor en la función \texttt{lista\_compuesta}). Devuelve una lista de tuplas. En la Figura \ref{CEnormal} se puede observar que no se muestra la propiedad donde se produce el contra--ejemplo. 
		\begin{figure}[h]
		\centering
		\includegraphics[width=10cm]{imagenes/CEnormal.PNG}
		\caption{Contra--ejemplo normal.}
		\label{CEnormal}
		\end{figure}
	\item \texttt{lista\_compuesta/2}:	La diferencia entre esta función y la función \texttt{lista\_simple} radica en que ésta recoge el nombre de la propiedad donde se produjo el contra--ejemplo. Se hace una llamada a \texttt{lista\_simple} pasándole como tercer parámetro, el nombre de la propiedad recogida. Acepta dos parámetros (una lista y  otra vacía) y devuelve una lista de tuplas. En la Figura \ref{CEmodule} se puede observar que sí se muestra la propiedad, por ello, esta función tiene que ser ligeramente diferente.
		\begin{figure}[h]
			\centering
			\includegraphics[width=13cm]{imagenes/CEmodule.PNG}
			\caption{Contra--ejemplo con propiedad.}
			\label{CEmodule}
		\end{figure}
	\item \texttt{interpretar\_procesos/2}: Función donde se guardan los datos. Recibe una tupla y el nombre de la propiedad y devuelve una tupla personalizada. Cada elemento de la lista de la Figura \ref{read_formato} sería la tupla.
\end{itemize}
	\begin{figure}[h]
		\centering
		\includegraphics[width=14cm]{imagenes/read_formato.PNG}
		\caption{Lista de tuplas que aceptará el módulo \emph{Formato}.}
		\label{read_formato}
	\end{figure}

Para hacer uso de las funciones que recogen los contra--ejemplos, es necesario incluir la librería de QuickCheck (``\texttt{eqc/include/eqc.hrl}''). La lista de tuplas que aceptará el módulo \texttt{Formato} se muestra en la Figura \ref{read_formato}.
	
La única función pública es \texttt{contraejemplo/0} las demás se exportan únicamente para posibilitar la ejecución de las pruebas de unidad. 

A continuación se muestra un diagrama de secuencias de ejecución del módulo (ver  \ref{diaread}). En el primer caso, si se ejecutaron las pruebas con \texttt{eqc:module} la llamada a la función \texttt{eqc:counterexamples} devolverá el contra--ejemplo creado. En el segundo caso, si no se ejecutaron las pruebas con \texttt{eqc:module} devolverá un \texttt{undefined} y se realiza una llamada a \texttt{eqc:counterexample} el cual devolverá un contra--ejemplo. (Para más información del uso de estas dos funciones, ver el apartado referente a QuickCheck).
	\begin{figure}[htbp]
		\centering
		\subfigure{\includegraphics[width=70mm]{imagenes/diaread1.pdf}}
		\subfigure{\includegraphics[width=70mm]{imagenes/diaread2.pdf}}
		\caption{Secuencias de ejecución del módulo \emph{Read}.} 
		\label{diaread}
	\end{figure}

\subsection{Módulo de transformación (\textit{Formato})}
La función principal de este módulo es recoger la lista de tuplas creada en el módulo \texttt{Read} e interpretarla, identificando actores, participantes, propiedades, creación de la estructura principal,etc.
Las funciones o responsabilidades son las siguientes:
\begin{itemize}
	\item \texttt{leer\_contraejemplo/0} y \texttt{leer\_contraejemplo/1}: Son las funciones principales de este módulo. En el primer caso, no acepta ningún parámetro y en el segundo caso, un parámetro que será el nombre de fichero escogido por el usuario. Esta función llamará a \texttt{generar\_contraejemplo/1}.	
	\item \texttt{generar\_contraejemplo/1}: Es la función donde se realiza una llamada al módulo \texttt{Read} para recoger el contra--ejemplo y a continuación interpretar los datos. Se inicializa el módulo \texttt{Supervisor\_format} y se envía al módulo \texttt{Write\_text} una tupla de listas personalizada (ver Figura \ref{formato_write_text}) además del nombre del fichero. 
	\item \texttt{modificar\_valor/2}: Esta función modifica el valor de devolución de la llamada por \texttt{\{error,bagarg\}} donde se produjo el contra--ejemplo. Así sabremos dónde llega la secuencia. Acepta como parámetros una lista y otra lista vacía. Devuelve la lista modificada.
	\item \texttt{leer\_spawn/2}: Esta función lee la llamada \texttt{spawn} de la lista para luego buscar la llamada \texttt{Register} y así recoger el nombre del proceso. Esto es necesario porque se realiza una llamada a la función \texttt{buscar\_registro} que se explica a continuación. Acepta una lista y otra vacía y devuelve una lista personalizada.
	\item \texttt{buscar\_registro/3}: En esta función se busca el nombre del cual se va registrar el proceso y crear ese participante. Acepta una lista y otra vacía, además de un elemento (Figura \ref{diaDividido}). Después de realizar el \texttt{spawn}, se realiza una llamada  \texttt{creates} al participante \texttt{a} desde el participante que respondió la llamada \texttt{spawn} y a continuación, se siguen la ejecución con normalidad.	
	\item \texttt{leer\_actores/2} y \texttt{leer\_actores\_aux/2}: En estas funciones, se leen los actores de la lista y se crea una lista de actores sin repetir. Acepta una lista y otra vacía, devuelve la lista de actores.	
	\item \texttt{leer\_participantes/2} y \texttt{leer\_participantes\_aux/2}: En estas funciones, se leen los participantes de la lista y se crea una lista de participantes sin repetir. Acepta una lista y otra vacía, devuelve la lista de participantes.	
	\item \texttt{leer\_propiedades/2} y \texttt{leer\_propiedades\_aux/2}:  En estas funciones, se leen los propiedades de la lista y se crea una lista de propiedades sin repetir. Acepta una lista y otra vacía, devuelve la lista de propiedades.
\end{itemize}

Se ha creado una librería para evitar repetir valores y poder definirlos en un lugar desde donde se puedan reutilizar en todos los módulos de la implementación que lo necesiten la cual va ser necesario incluirla (``\texttt{../include/gadsuce.hrl}''). La tupla de listas que se mandará al módulo \texttt{Write\_text} se muestra en la Figura  \ref{formato_write_text}.

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{imagenes/formato_write_text.PNG}
	\caption{Tupla de listas que aceptará el módulo \emph{Write\_text}.}
	\label{formato_write_text}
\end{figure}

La única función pública es \texttt{leer\_contraejemplo/0} y \texttt{leer\_contraejemplo/1} las demás sólo se exportan para ejecutar las pruebas de unidad.

A continuación se muestra un diagrama de secuencia de ejecución del módulo (ver \ref{diaformato}). La primera llamada \texttt{leer\_contraejemplo()} también acepta un parámetro. No se ha puesto por simplicidad. El diagrama está separado por funcionalidades para mejorar el entendimiento. Se presupone que la llamada al módulo \texttt{Read} con la función \texttt{contraejemplo} devolverá \texttt{true}.

\begin{figure}[h]
	\centering	
	\includegraphics[width=13cm]{imagenes/diaformato.pdf}
	\caption{Secuencia de ejecución del módulo \emph{Formato}.}
	\label{diaformato}
\end{figure}

\clearpage
\subsection{Módulo de escritura (\emph{Write\_text})}
La función principal de este módulo, es crear el texto compatible que PlantUML interpretará, y se llamará al módulo \texttt{Runner} para que lea el texto compatible y genere el diagrama de secuencia UML. Si se usase otra herramienta de generación de diagramas, habría que adaptar este módulo o crear uno nuevo.

Las principales funciones de este módulo son:

\begin{itemize}
	\item \texttt{write\_file/2}: Función principal donde se gestionará la creación de texto compatible, y se hará una llamada al módulo \texttt{Runner}  para crear el diagrama de secuencia UML. Acepta la tupla de listas (Figura  \ref{formato_write_text}) y un nombre del fichero. Devuelve un \texttt{ok} o un error \texttt{\{error,Motivo\}}.
	
	\item \texttt{modificar\_nombre/1}: Función que modifica el nombre del fichero para que nunca se repita. En caso de que se repita, se añade \texttt{\_num}, siendo \texttt{num} un número empezando por el 1. Acepta un parámetro que será el nombre del fichero.
	
	\item \texttt{conversion/2}: Función donde se crea la estructura del fichero. En esta función se van llamando a otras funciones que serán las encargadas de dicha creación. Acepta un fichero y la tupla de listas como parámetros.
	
	\item \texttt{conversion\_aux/3}: En esta función se escribe la mayor parte del contenido del texto compatible, además de comprobar la lista de propiedades. En caso de que haya propiedades diferentes, se adjunta al texto compatible una opción para dividir los diagramas de secuencia UML. Acepta un fichero y dos listas.
	
	\item \texttt{imprimir\_creates/2}: Esta función se usa para imprimir la llamada \texttt{creates} que realiza una llamada a un participante creado por la función \texttt{spawn}.
	
	\item \texttt{flecha\_vuelta/3}: Se escribe en el texto compatible la flecha de vuelta que consta de un actor y un participante. Acepta como parámetros un fichero, un actor y un participante.
	
	\item \texttt{flecha\_ida/3}: Lo mismo que la función anterior.
	
	 \item \texttt{participantes/2}: Se escribe en el texto compatible la lista de participantes. Acepta el fichero y la lista de participantes.
	 
	 \item \texttt{actores/2}: Se escribe en el texto compatible la lista de actores. Acepta el fichero y la lista de actores.
	 
	 \item \texttt{fin/1}: Se escribe \texttt{@startuml} en el texto compatible. Acepta el fichero.
	 
	 \item \texttt{opciones/2}: Se escriben las opciones  \texttt{autonumber} y \texttt{title} en el texto compatible, las cuales numera las secuencias y ponen el título de la propiedad que ha fallado, respectivamente. Acepta el fichero y el nombre de la propiedad.
	 
	 \item \texttt{inicio/1}: Se escribe \texttt{@enduml} en el texto compatible. Acepta el fichero.
	 
	 \item \texttt{activar/2}: Activa la ejecución.
	 
	 \item \texttt{desactivar/2}: Desactiva la ejecución.
	 
\end{itemize}
Se ha creado una librería para evitar repetir valores y poder definirlos en un lugar desde donde se puedan reutilizar en todos los módulos de la implementación que lo necesiten la cual va ser necesario incluirla (``\texttt{../include/gadsuce.hrl}'').

La única función pública es \texttt{write\_file/2}, las demás aunque se exportan, es sólo para ejecutar las pruebas de unidad.

A continuación se muestra un diagrama de secuencia del módulo (ver \ref{diawrite}).
\begin{figure}[h]
	\centering
	\includegraphics[width=11cm]{imagenes/diawrite.pdf}
	\caption{Secuencia de ejecución del módulo \emph{Write\_Text}.}
	\label{diawrite}
\end{figure}
\clearpage
\subsection{Módulo de supervisión (\emph{Supervisor\_format})}
La función principal de este módulo es monitorizar los diferentes procesos que se encargarán en tiempo de ejecución, de las funcionalidades de los módulos identificados en la aproximación inicial. El supervisor es el responsable de iniciar, detener y monitorizar dichos procesos, manteniéndolos vivos y reiniciándolos cuando sea necesario.

El inicio del supervisor será en el módulo de transformación (\texttt{Formato}) y el supervisor a su vez creará el proceso hijo \texttt{Runner} el cual va a monitorizar.
En el módulo de escritura, se hará una llamada al proceso \texttt{Runner} y se creará el diagrama UML a partir del texto compatible.

Las funciones principales de las que está compuesto este módulo son:
\begin{itemize}
	\item \texttt{start/0}: Crea un proceso supervisor como parte de un árbol de supervisión. LLama a la función \texttt{start\_link/2,3} y se asegura que el supervisor está vinculado al proceso de llamada. La creación del proceso supervisor llama a la función \texttt{init} para averiguar acerca de la estrategia de reinicio, máxima intensidad de reinicio y los procesos secundarios. 	
	\item \texttt{init/1}: Función principal que configurará el supervisor y el proceso hijo mediante \texttt{flags}. 
\end{itemize}
En la contextualización, se ha comentado todas las opciones posibles del supervisor y del proceso hijo. En la Figura \ref{confSup} se puede ver la configuración de nuestro supervisor.
\begin{figure}[h]
	\centering
	\includegraphics[width=14cm]{imagenes/confSup.PNG}
	\caption{Configuración del \emph{Supervisor}.}
	\label{confSup}
\end{figure}

\subsection{Módulo de ejecución (\emph{Runner})}
La función principal de este módulo es la ejecución del archivo \texttt{java} \texttt{plantuml.jar} que leerá el texto compatible y creará el diagrama UML asociado a ese texto. Este módulo es monitorizado y/o reiniciado por el módulo \texttt{Supervisor\_format} y ejecutado por el módulo \texttt{Write\_text}.

Este módulo se ha configurado como un \texttt{gen\_server} ya que proporciona por gestionar un número indeterminado de procesos hijo.

Las funciones principales del módulo \texttt{gen\_server}:
\begin{itemize}
	\item \texttt{iniciar}: Inicia el proceso hijo desde el módulo \texttt{Supervisor\_format}. En el momento de ejecutarse esta función, llamará a la función del módulo \texttt{callback} llamada \texttt{init}.
	\item \texttt{exe}: Esta función llama a la función \texttt{cast} del módulo para generar el diagrama. En el momento de ejecutarse esta función, llamará a la función del módulo \texttt{callback} llamada \texttt{handle\_cast} la cual se explica más adelante. Se le pasa un parámetro que será el nombre del fichero a leer.
\end{itemize}

En el apartado de contextualización, hemos mostrado la relación entre las funciones propias del \emph{behaviour} y el módulo \texttt{callbak} (ver Figura \ref{genserver} en este caso).
Las funciones principales del módulo \texttt{callback} de este \emph{behaviour} son:
\begin{itemize}
	\item \texttt{init}: Función que es llamada cuando se inicia el proceso hijo.
	\item \texttt{handle\_cast}: Función llamada por la función \texttt{cast}. Se podría traducir como \emph{``manejador de la función cast''}. Aquí se crea el diagrama de secuencia UML. Primero se comprueba que esté el ejecutable \texttt{plantuml.jar} en el directorio correspondiente, en caso afirmativo se crea el diagrama.
	\item \texttt{handle\_call}: Función llamada por la función \texttt{call}. \emph{``Manejador de la función call''}, no usada pero necesario poner la cabecera.
	\item \texttt{handle\_info}: Función llamada por la función \texttt{cast}. \emph{``Manejador de la función info''}, no usada pero necesario poner la cabecera.
	\item \texttt{code\_change}: Función llamada por la función \texttt{cast}. \emph{``Código cambia''}, no usada pero necesario poner la cabecera.
	\item \texttt{terminate}: Función llamada por la función \texttt{cast} o la función \texttt{stop}. \emph{``Terminado''}, no usada pero necesario poner la cabecera.
\end{itemize}

