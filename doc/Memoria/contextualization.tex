\chapter{Contextualización}
\minitoc
\label{chap:contextualizacion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Contar cómo estaba la situación antes de empezar,                  %
%           todo lo que se hizo para familiarizarse con las tecnologías,       %
%           casarlas, etc. 
%En la contextualización, una sección debe explicar paso a paso con un ejemplo cómo %son las pruebas basadas en propiedades con QuickCheck. Puedes usar cualquier %ejemplo sencillo que tengas a mano, si tienes dudas, %pregúntame                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine{E}{n} este capítulo se explican en profundidad cada una de las tres herramientas fundamentales utilizadas en este proyecto: la herramienta de generación de diagramas PlantUML, el lenguaje de implementación Erlang y sus librerías OTP, y la herramienta basada en pruebas (PBT) QuickCheck.

\section{PlantUML}
 \textbf{PlantUML} es un proyecto de código abierto desarrollado bajo licencia GPL que permite crear fácil y rápidamente:
 \begin{itemize}
 	\item Diagramas de secuencia.
 	\item Diagramas de casos de uso.
 	\item Diagramas de clases.
 	\item Diagramas de actividades.
 	\item Diagramas de componentes.
 	\item Diagramas de estados.
 	\item Diagramas de objetos.
 \end{itemize} 
 
Dichos diagramas son definidos usando un simple fichero de texto, donde se describen los componentes del diagrama y sus relaciones en un lenguaje simple e intuitivo. Para este proyecto, usaremos PlantUML para la generación de diagramas de secuencia. Primero se tendrá que crear un texto compatible con la nomenclatura de PlantUML y luego proceder a la invocación de la herramienta. PlantUML está desarrollada en Java, por lo se que necesita disponer de una máquina virtual para este lenguaje, pero no tiene dependencias con ninguna versión en particular. Para la creación del diagrama, tenemos que tener el ejecutable \emph{plantuml.jar} y el texto compatible. No es necesario que estén en la misma ruta ya que éstas se pueden indicar durante la ejecución. Abriendo un terminal ponemos lo siguiente:
\begin{center} 
	\texttt{java -jar plantuml.jar texto\_compatible}
\end{center}

Lo cual generará el fichero \texttt{texto\_compatible.png} en el mismo directorio donde se encuentra \texttt{texto\_compatible}, conteniendo el diagrama UML descrito en éste (siempre que la definición no contenga errores de sintaxis). Alternativamente, se puede ejecutar de manera interactiva:

\begin{center} 
	\texttt{java -jar plantuml.jar}
\end{center}

Y navegar hasta la ubicación de \texttt{texto\_compatible}\footnote{La extensión esperada por PlantUML para los ficheros de definición de diagramas es \texttt{.pu}}, para realizar una previsualización (ver Figura \ref{PlantUMLinteractivo}).

 \begin{figure}[hp]
 	\centering
 	\includegraphics[width=0.9\textwidth]{imagenes/plantUMLinteractivo.png}
 	\caption{Ejecución de PlantUML en modo interactivo.}
 	\label{PlantUMLinteractivo}
 \end{figure} 

El primer ejemplo que aparece en el manual de PlantUML se corresponde con la definición del texto compatible y a continuación con el diagrama de secuencia que se genera a partir de la definición proporcionada. Se puede ver la simpleza del texto compatible en la Figura \ref{codigoEJplant}, y su representación en la Figura \ref{diagramaEJplant}:
 
 \begin{figure}[hp]
 	\centering
 	\includegraphics[width=12cm]{imagenes/codigoEJplant.PNG}
 	\caption{Ejemplo código texto compatible.}
 	\label{codigoEJplant}
 \end{figure} 
  \begin{figure}[h]
  	\centering
  	\includegraphics[width=10cm]{imagenes/diagramaEJplant.pdf}
  	\caption{Ejemplo de diagrama de secuencia.}
  	\label{diagramaEJplant}
  \end{figure}

 \begin{itemize}
        \item Marca de inicio: \texttt{@startuml}.
 	\item Un emisor: \texttt{Alice}.
 	\item Un receptor: \texttt{Bob}.
 	\item Un mensaje: \texttt{Authentication Request}.
 	\item Marca de fin: \texttt{@enduml}.
 \end{itemize}
 
Nótese que no es necesario declarar como \emph{partipant} el emisor y el receptor antes de usarlos por primera vez para declarar un mensaje.

 Además, se puede exportar como PNG (por defecto) o SVG y XML está en fase beta. Con cualquier visor de formatos gráficos, se podría ver el diagrama, ya que PNG es un estándar ampliamente extendido y reconocido por su alta compresión y bajo peso.

\section{Erlang/OTP}

Este proyecto está desarrollado íntegramente en Erlang~\cite{erlang}. Erlang es un lenguaje funcional orientado a la concurrencia. Fue creado por Joe Armstrong con la ayuda de algunos compañeros en los laboratorios Ericsson en los años ochenta, y liberado como código abierto a finales de los noventa. Las características más destacables de este lenguaje de programación en contraposición con otros lenguajes imperativos u orientados a objetos son:
 
 \begin{itemize}
 	\item \textbf{Distribuido}: No se produce todo el procesado de datos en un mismo sistema, si no que se \textit{``distribuye''} a lo largo de varios sistemas. De este modo, permite mayor procesado sin apenas carga. Aunque naturalmente la distribución debe ser programada, en Erlang se trata igual un mensaje a/de un proceso que está en el mismo nodo como a otro que proviene de un nodo diferente, por ello se dice que la distribución es transparente.
 	Un ejemplo de un sistema distribuido es el programa del \textbf{SETI} (\textit{``Search for Extraterrestrial Intelligence''}) llamado \textbf{SETI@home} cuya finalidad es la utilización de los ordenadores personales de los usuarios, quienes quieran contribuir a la búsquedad de vida inteligente fuera del planeta tierra, ejecutando gran cantidad de procesos a lo largo de todos los participantes.
 	
 	\item \textbf{Tolerancia a fallos}: Es muy engorroso cuando una parte del programa falla y como consecuencia de eso, el programa se interrumpe de forma completa. En Erlang si una parte del programa produce algún error, no significa que el sistema se detenga ya que Erlang sigue una filosofía \emph{``let it crash''} (dejar que rompa). Esto significa que en lugar de programar a la ``defensiva'', se programan mecanismos de supervisión para monitorizar los procesos y en caso de que rompan, volver a levantarlos.
  	
 	\item \textbf{Escalabilidad}: Erlang puede gestionar un número de procesos muy significativo (cientos de miles o incluso millones) sin pérdida de calidad, en contraposición a otros lenguajes y/o sistemas. La clave es que los procesos Erlang son procesos de la máquina virtual sobre la que se ejecuta el lenguaje, que no tienen una correspondencia 1:1 con los procesos del sistema operativo subyacente, sino que se gestionan muy eficientemente por dicha máquina virtual, que aprovecha perfectamente las capacidades del hardware subyacente en CPUs de hasta cientos de cores.
          
 	\item \textbf{Cambio de código en caliente}: En sistemas críticos o de producción donde no es una opción parar el sistema, en muchas ocasiones, es necesario introducir actualizaciones para la mejora del rendimiento o ampliar funcinalidades. Erlang lo permite, ya que su máquina virtual incluye un servidor de código que se encarga de actualizar el código en memoria que cada proceso está ejecutando de manera transparente y segura para éstos.
 	
 	\item \textbf{Orientado a la concurrencia}: El funcionamiento más interno de Erlang está preparado para el uso de la concurrencia de manera explícita, a través del paso de mensajes asíncrono, y transparente. \\
 \end{itemize}
 
 Erlang se distribuye acompañado de OTP (\emph{Open Telecom Platform}), que es un conjunto de librerías y principios de diseño para desarrollo de sistemas \textit{middle-ware}. Incluye además, su propia base de datos distribuida, librerías para depuración, etc.
 
 Aunque cumple en 2016 los 30 años de edad, Erlang se ha hecho famoso en el nuevo milenio. La razón es que se ha empezado a usar en sectores como por ejemplo el bancario, o el de los videojuegos, además de en sus sectores tradicionales como son el de las telecomunicaciones, o el del comercio electrónico. Son casos de éxito especialmente conocidos:
 \begin{itemize}
 	\item \textbf{Facebook}: Lo han usado para la implementación de su chat.
 	\item \textbf{WhatsApp}: Es la tecnología principal del \emph{back-end} de su servicio de intercambio de mensajes.
 	\item \textbf{SpilGames, Demonware}: Lo han usado para soportar gran cantidad de usuarios conectados simultáneamente en partidas on-line en juegos tan conocidos como \emph{``Call of Duty''}.
 \end{itemize}
 \subsection{Patrones de diseño: \emph{behaviours} (comportamientos)}
 De forma general, en Diseño Software es buena idea el uso de patrones de diseño ya que proporcionan soluciones para problemas habituales y recurrentes que nos podemos encontrar cuando desarrollamos software.
 
 Aunque cada aplicación es única, es muy probable que tenga partes comunes con otras aplicaciones como acceso a datos, creación de objetos, operaciones entre sistemas, etc.
 En lugar de reinventar la rueda, podemos solucionar estos problemas utilizando algún patrón, ya que son soluciones probadas y documentadas, usadas por multitud de programadores.
 
Muchas tecnologías adaptan la idea de patrón a su propio contexto, como en el caso de Erlang al proponer los ``behaviours''  que favorecen la construcción de sistemas más robustos y con menos errores.
 
 Los \textbf{\emph{behaviours}} o comportamientos de Erlang ofrecen funcionalidades reutilizables a través de un conjunto estándar de funciones de interfaz. La parte específica de la funcionalidad que \emph{instancia} cada \emph{behaviour} se implementa en un módulo que llamaremos de \emph{callback}. El módulo de \emph{callback} se encarga de implementar la interfaz determinada por el \emph{behaviour}. A su vez, el \emph{behaviour} se encargará de ejecutar la funcionalidad completa, mediante llamadas a dichas funciones en el módulo de \emph{callback}.
 
Uno de los \emph{behaviour} más utilizados en Erlang es el \textbf{Supervisor} que es un proceso que supervisa a otros procesos. Un proceso hijo puede ser otro supervisor o un ``worker'' (trabajador) que normalmente, se implementa utilizando alguno de los \emph{behaviour} \emph{gen\_event}, \emph{gen\_fsm} o \emph{gen\_server}.
 
 Los supervisores se utilizan para construir una estructura jerárquica de procesos llamada \emph{``árbol de supervisión''} (que es una buena idea para estructurar una aplicación tolerante a fallos). Además, es un proceso responsable de iniciar, detener y monitorizar otros procesos, manteniéndolos vivos y reiniciándolos cuando sea necesario. Para poder utilizarlo, es necesario configurar las propiedades del supervisor y las propiedades de cada proceso hijo. A continuación se muestran  las posibles opciones:
\begin{itemize}
	\item \texttt{SupFlags}: Definición de las propiedades del supervisor.
	\begin{itemize}
		\item \texttt{strategy}: Estrategias de reinicio de los procesos hijo.
		\begin{itemize}
			\item \texttt{one\_for\_one}: Si el proceso hijo termina y es reiniciado, solo afecta a ese proceso hijo. Es la estrategia de reinicio predeterminada.
			\item \texttt{one\_for\_all}: Si un proceso hijo termina y es reiniciado, todos los procesos se fuerzan a terminar y son reiniciados en bloque.
			\item \texttt{rest\_for\_all}: Si un proceso hijo termina y es reiniciado, el ``resto'' de los procesos hijo (esto es, los hermanos creados después del hijo afectado) son forzados a terminar y reiniciados en bloque siguiendo el orden de creación.
			\item \texttt{simple\_one\_for\_one}: Es como la estrategia \texttt{one\_for\_one} pero más simplificada en la que todos los procesos son añadidos de forma dinámica siendo el mismo tipo de proceso y ejecutan el mismo código.
		\end{itemize}
		\item \texttt{intensity}: Se corresponde con el número de reinicios tolerado por período de tiempo, por defecto es 1. Si se supera la \emph{intensidad} de fallos, el supervisor desiste de su tarea y todo el árbol de supervisión (supervisor e hijos), se detienen.
		\item \texttt{period}: Se corresponde con el intervalo con respecto al que se mide la \emph{intensidad} de los reinicios, por defecto son 5 segundos. Así, \texttt{intensity} y \texttt{period} trabajan juntas para evitar repeticiones infinitas de procesos terminados y reiniciados.
	\end{itemize}
	\item \texttt{ChildSpecs}: Definición de las propiedades de los procesos hijos.
	\begin{itemize}
		\item \texttt{id}: Se usa para identificar la especificación del proceso hijo por el supervisor.
		\item \texttt{start}: Define la función que será utilizada para iniciar el proceso hijo.
		\item \texttt{restart}: Define cuándo el proceso hijo tiene que ser reiniciado. 
		\begin{itemize}
			\item \texttt{permanent}: El proceso hijo se reinicia siempre. Opción por defecto.
			\item \texttt{temporary}: El proceso hijo no se reinicia nunca, incluso si la estrategia es \texttt{one\_for\_all} o \texttt{rest\_for\_one} y es la muerte de un proceso hermano la causa de que el proceso temporal termine.
			\item \texttt{transient}: El proceso hijo se reinicia sólo si terminó de forma anormal.
		\end{itemize}					
		\item \texttt{shutdown}: Define cómo se fuerza la terminación de un proceso hijo. Puede terminase de forma incondicional con \texttt{brutal\_kill}, o bien el supervisor puede enviar una señal de terminación ordenada al hijo, lo que se indica con un entero que representa el \emph{timeout} (en segundos) que espera el supervisor por la terminación de ese hijo. Si en lugar de un entero se indica \texttt{infinity}, el supervisor esperará el tiempo que haga falta a que el hijo termine.
		\item \texttt{type}: Aquí indicará si el proceso hijo va ser un supervisor o un \emph{``worker'' (trabajador)}. Este \emph{flag} está relacionado con el anterior, ya que en caso de que el hijo sea otro supervisor, se recomienda que \texttt{shutdown} sea \texttt{infinity} para permitir que cada subrama de supervisión aplique su propia política.
		\item \texttt{modules}: Es usado por el controlador durante el reemplazo del código para determinar qué procesos están utilizando un determinado módulo.
	\end{itemize}
\end{itemize}

Además del supervisor, existen otros tres \emph{behaviours} en Erlang:

\begin{itemize}
	\item \texttt{gen\_fsm} (\emph{generic finite state machine}): Este módulo de comportamiento proporciona una máquina de estados finita. La relación entre las funciones del \emph{behaviour} y el módulo \emph{callback} se pueden ver en la Figura \ref{genfsm}.
	\begin{figure}[h]
		\centering
		\includegraphics[width=13cm]{imagenes/genfsm.PNG}
		\caption{Funciones del API del comportamiento \texttt{gen\_fsm}.}
		\label{genfsm}
	\end{figure}	
	
	\item \texttt{gen\_event} (\emph{generic event handling}): Este módulo de comportamiento proporciona la funcionalidad de manejo de eventos. Consiste en un proceso gestor de eventos genérico y cualquier número de eventos será añadido o borrado dinámicamente. La relación entre las funciones del \emph{behaviour} y el módulo \emph{callback} se pueden ver en la Figura \ref{genevent}. 	

	\begin{figure}[h]
		\centering
		\includegraphics[width=13cm]{imagenes/genevent.PNG}
		\caption{Funciones del API del comportamiento \texttt{gen\_event}.}
		\label{genevent}
	\end{figure}	
	
	\item \texttt{gen\_server} (\emph{generic server}): Este módulo de comportamiento proporciona el servidor en una relación entre un cliente--servidor.
	La relación entre las funciones del \emph{behaviour} y el módulo \emph{callback} se pueden ver en la Figura \ref{genserver}.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=13cm]{imagenes/genserver.PNG}
		\caption{Funciones del API del comportamiento \texttt{gen\_server}.}
		\label{genserver}
	\end{figure}
	
\end{itemize}

\clearpage

\section{QuickCheck}
Es una herramienta de pruebas basadas en propiedades (PBT), comercializada por la empresa Quviq, que genera y ejecuta casos de prueba aleatorios automáticamente. Se inspira en la original, nacida con el mismo nombre en el seno de la comunidad de Haskell en 1999, y es la más completa de todos los clones para diferentes lenguajes que han ido surgiendo desde entonces (incluyendo Java, C, C++, OCaml, etc.).

Existen varias formas de realizar las pruebas automáticas con QuickCheck:
\begin{itemize}
	\item \texttt{eqc:quickcheck/1}: Recibe como argumento una propiedad concreta, por lo que se realizarían las pruebas por cada propiedad de forma individual. Si en un módulo de pruebas hay implementadas muchas propiedades de prueba, sería engorroso tener que probar una a una. Haciendo alusión al ejemplo de pruebas mostrado en la introducción, realizamos las pruebas (Figura ~\ref{test1}).		
	\item \texttt{eqc:module/1}: A diferencia de lo comentado anteriormente, esta función recibe como argumento un nombre de módulo, por lo que realizaría las pruebas de cada propiedad implementada en bloque, ayudando mucho al \textit{testing} (Figura ~\ref{test2}).
\end{itemize}

		\begin{figure}[h]
			\centering
			\includegraphics[width=10cm]{imagenes/test1.PNG}
			\caption{Ejemplo prueba con \texttt{eqc:quickcheck/1}}
			\label{test1}
		\end{figure}	
		\begin{figure}[h]
			\centering
			\includegraphics[width=10cm]{imagenes/test2.PNG}
			\caption{Ejemplo prueba con \texttt{eqc:module.}}
			\label{test2}
		\end{figure}

QuickCheck tiene implementadas en su librería diferentes funciones para manipular los contra--ejemplos que genera cuando la ejecución de casos de prueba aleatorios producen un ejemplo que invalida la propiedad que se está intentando validar:

\begin{itemize}
	\item \texttt{eqc:counterexample/0}: La función más básica, la cual recoge el último contra--ejemplo encontrado. En caso de que se hayan ejecutado varias propiedades, con sus respectivos contra--ejemplos, sólo devolverá el contra--ejemplo de la última propiedad ejecutada.
	
	\item \texttt{eqc:counterexample/1}: A esta función se le pasa el nombre de la propiedad como parámetro, como por ejemplo:	
	\begin{center} 
		\texttt{eqc:counterexample(reg\_eqc:prop\_registration())}.
	\end{center}
	
	La principal diferencia es que esta función recupera el contra--ejemplo de la propiedad pasada como parámetro (en caso de haber sido generado durante la sesión actual de la máquina virtual). Hace lo mismo que la función \texttt{eqc:quickcheck/1}, pero además muestra el contra--ejemplo:
	
	\item \texttt{eqc:counterexample/2}: Equivalente a la anterior, pero además se le pueden pasar opciones como parámetro, por ejemplo la tupla \texttt{\{with\_info, true\}}, que nos devolvería una estructura que contiene más campos con información muy interesante:
	\begin{itemize}
		\item \texttt{result}: El contra--ejemplo original.
		\item \texttt{statistics}: Un mapa que contiene:
		\begin{itemize}
			\item Resultado de la prueba.
			\item Número de pruebas ejecutadas.
			\item Pruebas descartadas.
			\item Si las pruebas fallan, también muestra los pasos satisfactorios y fallidos del \emph{shrinking}.
		\end{itemize} 
		\item \texttt{agregated\_data}: Todos los datos agregados que son etiquetados con \texttt{with\_tag} o \texttt{with\_title}.
		\item \texttt{measurements}: Colección de datos realizada sobre los datos generados para las pruebas.
		\item \texttt{user\_info}: Colección de datos personalizable por el usuario.
	\end{itemize}
	
	\item \texttt{eqc:counterexamples/0}: Esta función devuelve una lista de tuplas donde el primer elemento es la propiedad donde se producen los contra--ejemplos y el segundo elemento una lista con los contra--ejemplos de cada propiedad. La diferencia con las anteriores funciones radica en que las pruebas \textbf{\emph{deben}} haberse invocado con la función \texttt{eqc:module/1}. En caso contrario no devolvería nada, o devolvería el último contra--ejemplo encontrado en la última ejecución de la función \texttt{eqc:module/1}.
	
	\item \texttt{eqc:current\_counterexample/0}: Devuelve el último contra--ejemplo encontrado por \texttt{eqc:quickcheck/1}. Además, genera un fichero binario donde almacena el contra--ejemplo, de manera que se puede recuperar entre sesiones diferentes de la máquina virtual.
\end{itemize}

En resumen, todas las funciones de recolección de contra--ejemplos exceptuando \texttt{eqc:counterexamples/0} necesitan que se hayan invocado las pruebas con 
\texttt{eqc:quickcheck/1}. La función \texttt{eqc:counterexamples/0} necesita que la generación de pruebas se haya realizado con \texttt{eqc:module/1}. \\

En la Figura~\ref{error1} se puede apreciar el uso de las diferentes funciones.
\begin{enumerate}
	\item Primero se realizan las pruebas con:
	\begin{center}
		\texttt{eqc:quickcheck(reg\_eqc:prop\_registration())}
	\end{center}
	QuickCheck detecta un error y procede a generar un contra--ejemplo. Se puede ver el poder del proceso no determinista de \textit{shrinking}, que consiste en quitar pasos de la secuencia y ver si la nueva secuencia sigue fallando con el mismo error. Las ``x'' marcan los intentos en los que el QuickCheck quita algún paso pero no consigue reproducir el error. Se puede ver que se redujo de 7 pasos a 3 pasos, lo cual será interesante para entender mejor el contra--ejemplo, ya que tendrá un menor tamaño.
	\item A continuación se llama a la función
	\begin{center}
		\texttt{eqc:counterexamples()}
	\end{center}
	La cual devuelve un contra--ejemplo generado anteriormente y no corresponde con el creado en este ejemplo.
	\item Las siguientes llamadas a otras de las funciones mencionadas devuelven el mismo contra--ejemplo.	
\end{enumerate}

\begin{figure}[hp]
	\centering
	\includegraphics[width=\textwidth]{imagenes/error1.png}
	\caption{Ejemplos de recolección de contra--ejemplos.}
	\label{error1}
\end{figure}

 \clearpage
\section{Otras alternativas}
Los diagramas de secuencias UML son usados de forma intensiva en las pruebas software y en particular, en las pruebas de automatización por su sencillez y fácil compresión además de ser un estándar muy extendido.

En nuestro caso, hemos usamos los diagramas de secuencia UML como modelo de verificación de contra--ejemplos ya que nos ayuda a conocer la secuencia exacta y encontrar rápidamente donde se produce el problema.

Otros usos que la comunidad de investigación de pruebas software ha encontrado al uso de diagramas de secuencia UML incluyen la ingeniería inversa que mida la cobertura, pruebas de regresión, pruebas de estrés, pruebas de concurrencia e incluso como medición para comparar conjuntos de pruebas.

Como alternativa al uso de \emph{PlantUML} se podría pensar en \emph{MagicDraw} o \emph{Visio} como herramientas profesionales de modelado o también \emph{DiaUml}, o \emph{ArgoUml}. Las dos primeras son de pago y las otras son libres con licencia GNU GPL. Cada una se ajusta al estándar UML y tiene sus propias particularidades a la hora de usar un formato de salida (XML, binario, etc). Se ha escogido \emph{PlantUML} por su sencillez y rapidez a la hora de crear los diagramas con tan solo manejar texto plano.

Para el uso de herramientas PBT, una alternativa igual de válida sería \emph{PropER} que está basado en QuickCheck y trabaja bajo licencia GNU GPL. Se ha escogido \emph{QuickCheck} porque es una herramienta más avanzada que \emph{PropER}.


