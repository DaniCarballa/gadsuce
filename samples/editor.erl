%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2016
%%% @doc Validation code: editor process.
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU General Public License for more details.
%%% @end
%%%-------------------------------------------------------------------
-module(editor).
-behaviour(gen_fsm).

%% Public API
-export([new/1,
	 mode/1,
	 edit/2,
	 delete/1,
	 close/0]).
%% Internal API (gen_fsm)
-export([init/1, handle_event/3, terminate/3]).
-export([handle_info/3, handle_sync_event/4, code_change/4]).
%% Internal API (estados)
-export([inserting/2, inserting/3, overwriting/2, overwriting/3]).

%% Macro definition
-define(EDITOR, myed).

%%--------------------------------------------------------------------
%% @doc Inicia o editor.
%% @end
%% -------------------------------------------------------------------
-spec new(Filename :: string()) -> {ok, pid()}.
new(Filename) ->
    gen_fsm:start({local, ?EDITOR}, ?MODULE, Filename, []).

%%--------------------------------------------------------------------
%% @doc 
%% @end
%% -------------------------------------------------------------------
-spec mode(M :: atom()) -> ok.
mode(M) ->
    gen_fsm:send_event(?EDITOR, {mode, M}).

%%--------------------------------------------------------------------
%% @doc 
%% @end
%% -------------------------------------------------------------------
-spec edit(LineNumber :: pos_integer(), LineContents :: string()) -> non_neg_integer().
edit(LineNumber, LineContents) when LineNumber > 0 ->
    gen_fsm:sync_send_event(?EDITOR, {edit, LineNumber, LineContents}).
    
%%--------------------------------------------------------------------
%% @doc 
%% @end
%% -------------------------------------------------------------------
-spec delete(LineNumber :: pos_integer()) -> ok.
delete(LineNumber) when LineNumber > 0 ->
    gen_fsm:send_all_state_event(?EDITOR, {delete, LineNumber}).

%%--------------------------------------------------------------------
%% @doc Pecha o editor e grava o ficheiro resultante.
%% @end
%% -------------------------------------------------------------------
-spec close() -> ok.
close() ->
    gen_fsm:send_all_state_event(?EDITOR, stop).



%% ===== ===== ===== ===== ===== ===== ===== ===== =====

init(Filename) ->
    {ok, inserting, {Filename, []}}.

inserting({mode, insercion}, {Filename, FileLines}) ->
    {next_state, inserting, {Filename, FileLines}};
inserting({mode, sobreescritura}, {Filename, FileLines}) ->
    {next_state, overwriting, {Filename, FileLines}}.

inserting({edit, _LineNumber, LineContents}, _From, {Filename, []}) ->
    {reply, 1, inserting, {Filename, [LineContents]}};
%% este caso introduce un bug: non se actualiza o número de liña axeitadamente => o contraexemplo ten 1 mensaxe
%inserting({edit, 3, LineContents}, _From, {Filename, FileLines}) ->
%    {Head, Tail} = lists:split(2, FileLines),
%    {reply, length(FileLines), inserting, {Filename, Head ++ [LineContents] ++ Tail}};
inserting({edit, LineNumber, LineContents}, _From, {Filename, FileLines}) ->
    {Head, Tail} = lists:split(LineNumber-1, FileLines),
    {reply, length(FileLines)+1, inserting, {Filename, Head ++ [LineContents] ++ Tail}}.

overwriting({mode, sobreescritura}, {Filename, FileLines}) ->
    {next_state, overwriting, {Filename, FileLines}};
overwriting({mode, insercion}, {Filename, FileLines}) ->
    {next_state, inserting, {Filename, FileLines}}.

overwriting({edit, 1, LineContents}, _From, {Filename, []}) ->
    {reply, 1, overwriting, {Filename, [LineContents]}};
%% este caso introduce un bug: non se realiza a inserción => o contraexemplo ten 2 mensaxes
%overwriting({edit, 2, _LineContents}, _From, {Filename, FileLines}) ->
 %   {reply, length(FileLines), overwriting, {Filename, FileLines}};
overwriting({edit, LineNumber, LineContents}, _From, {Filename, FileLines}) ->
    {Head, [_|Tail]} = lists:split(LineNumber-1, FileLines),
    {reply, length(FileLines), overwriting, {Filename, Head ++ [LineContents] ++ Tail}}.

%% este caso introduce un bug: non se realiza o borrado => o contraexemplo ten 5 mensaxes
handle_event({delete, 4}, Estado, {Filename, FileLines}) ->
    {next_state, Estado, {Filename, FileLines}};
handle_event({delete, LineNumber}, Estado, {Filename, FileLines}) ->
    {Head, [_|Tail]} = lists:split(LineNumber-1, FileLines),
    {next_state, Estado, {Filename, Head ++ Tail}};
handle_event(stop, _Estado, {Filename, FileLines}) ->
    {stop, normal, {Filename, FileLines}}.

handle_info(_Info, NombreEstado, DatosEstado) ->
    {next_state, NombreEstado, DatosEstado}.

handle_sync_event(_Event, _From, NombreEstado, DatosEstado) ->
    {next_state, NombreEstado, DatosEstado}.

code_change(_Old, NombreEstado, DatosEstado, _) ->
    {ok, NombreEstado, DatosEstado}.

terminate(normal, _Estado, {Filename, FileLines}) ->
    {ok, Fd} = file:open(Filename, [write]),
    [ io:format(Fd, "~s~n", [Line]) || Line <- FileLines ],
    file:close(Fd);
terminate(_Other, _Estado, {Filename, FileLines}) ->
    {ok, Fd} = file:open(Filename, [write]),
    [ io:format(Fd, "~s~n", [Line]) || Line <- FileLines ],
    file:close(Fd).
