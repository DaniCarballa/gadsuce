%%% -*- coding: utf-8 -*-
%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2016
%%% @doc Validation model.
%%% @end
%%%-------------------------------------------------------------------
-module(editor_fsm).
-include_lib("eqc/include/eqc.hrl").
-include_lib("eqc/include/eqc_fsm.hrl").

-export([initial_state/0, initial_state_data/0, precondition/4,
	 postcondition/5, next_state_data/5, weight/3]).
-export([insert_mode/1, overwrite_mode/1]).
-export([prop_editor/0]).

-define(TEST_FILE, "test.txt").

%% @doc Definition of the state(s). Each state is represented by a function,
%%      listing the transitions from that state, together with generators
%%      for the calls to make each transition.
-spec insert_mode(S :: eqc_statem:symbolic_state()) ->
                         eqc_gen:gen({eqc_fsm:state_name(), eqc_statem:call()}).
insert_mode([] = S) ->
    [ {overwrite_mode, {call, editor, mode, [sobreescritura]}},
      {insert_mode, {call, editor, mode, [insercion]}},
      % 'history' denotes transitions that do not change the state
      {history, {call, editor, edit, [line_number(S), line_contents()]}}
    ];
insert_mode(S) ->
    [ {overwrite_mode, {call, editor, mode, [sobreescritura]}},
      {insert_mode, {call, editor, mode, [insercion]}},
      {history, {call, editor, edit, [line_number(S), line_contents()]}},
      {history, {call, editor, delete, [line_number(S)]}}      
    ].

overwrite_mode([] = S) ->
    [ {insert_mode, {call, editor, mode, [insercion]}},
      {overwrite_mode, {call, editor, mode, [sobreescritura]}},
      {history, {call, editor, edit, [line_number(S), line_contents()]}}
    ];
overwrite_mode(S) ->
    [ {insert_mode, {call, editor, mode, [insercion]}},
      {overwrite_mode, {call, editor, mode, [sobreescritura]}},
      {history, {call, editor, edit, [line_number(S), line_contents()]}},
      {history, {call, editor, delete, [line_number(S)]}}
    ].

line_number([]) ->
    return(1);
line_number(S) ->
    choose(1,length(S)).

line_contents() ->
    non_empty(list(printable_char())).

printable_char() ->
    elements([32,$a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o,$p,$q,$r,$s,$t,$u,$v,$w,$x,$y,$z,$1,$2,$3,$4,$5,$6,$7,$8,$9,$0]).

%% @doc Returns the <i>state name</i> of the initial state.
-spec initial_state() -> eqc_fsm:state_name().
initial_state() ->
    insert_mode.

%% @doc The initial state data.
-spec initial_eqc_fsm:state_data() -> eqc_fsm:state_data().
initial_state_data() ->
    [].

%% @doc Next state transformation for state data.
%%      Note: S is the current state, From and To are state names.
-spec next_state_data(From, To, S, V, Call) -> eqc_fsm:state_data()
    when From :: eqc_fsm:state_name(),
         To   :: eqc_fsm:state_name(),
         S    :: eqc_statem:symbolic_state(),
         V    :: eqc_statem:var(),
         Call :: eqc_statem:call().
next_state_data(insert_mode, insert_mode, S, _Var, {call, editor, edit, [LineNumber, LineContents]}) ->
    case lists:keyfind(LineNumber, 1, S) of
	false ->
	    lists:keysort(1, [{LineNumber, LineContents} | S]);
	_ ->
	    insert_and_displace({LineNumber, LineContents}, S, [])
    end;
next_state_data(overwrite_mode, overwrite_mode, S, _Var, {call, editor, edit, [LineNumber, LineContents]}) ->
    lists:keystore(LineNumber, 1, S, {LineNumber, LineContents});
next_state_data(_From, _To, S, _Var, {call, editor, delete, [LineNumber]}) ->
    delete_and_displace(LineNumber, S, []);
next_state_data(_From, _To, S, _Var, {call, _, _, _}) ->
    S.

%% @doc Precondition (for state data). Precondition is checked before
%%      command is added to the command sequence
-spec precondition(From, To, S, Call) -> boolean()
    when From :: eqc_fsm:state_name(),
         To   :: eqc_fsm:state_name(),
         S    :: eqc_statem:symbolic_state(),
         Call :: eqc_statem:call().
precondition(insert_mode, insert_mode, _S, {call, editor, mode, [sobreescritura]}) ->
    false;
precondition(overwrite_mode, overwrite_mode, _S, {call, editor, mode, [insercion]}) ->
    false;
precondition(insert_mode, overwrite_mode, _S, {call, editor, mode, [insercion]}) ->
    false;
precondition(overwrite_mode, insert_mode, _S, {call, editor, mode, [sobreescritura]}) ->
    false;
precondition(_From, _To, _S, _Call) ->
    true.

%% @doc Postcondition, checked after command has been evaluated
%%      Note: S is the state before next_state_data is evaluated.
-spec postcondition(From, To, S, Call, Res) -> true | term()
    when From :: eqc_fsm:state_name(),
         To   :: eqc_fsm:state_name(),
         S    :: eqc_statem:dynamic_state(),
         Call :: eqc_statem:call(),
         Res  :: term().
postcondition(insert_mode, insert_mode, S, {call, editor, edit, _}, TotalNumberOfLines) ->
    length(S)+1 =:= TotalNumberOfLines;
postcondition(overwrite_mode, overwrite_mode, [], {call, editor, edit, _}, TotalNumberOfLines) ->
    1 =:= TotalNumberOfLines;
postcondition(overwrite_mode, overwrite_mode, S, {call, editor, edit, _}, TotalNumberOfLines) ->
    length(S) =:= TotalNumberOfLines;
postcondition(_From, _To, _S, {call, _, _, _}, _Res) ->
    true.

%% @doc <i> Optional callback</i> Weight for transition(s).
%%      Specify how often each transition should be chosen
-spec weight(From, To, Call) -> integer()
    when From :: eqc_fsm:state_name(),
         To   :: eqc_fsm:state_name(),
         Call :: eqc_statem:call().
weight(_From, _To, {call, editor, edit, _}) ->
    60;
weight(_From, _To, {call, editor, delete, _}) ->
    30;
weight(_From, _To, {call, editor, mode, _}) ->
    10.

%% @doc Default generated property
-spec prop_editor() -> eqc:property().
prop_editor() ->
  ?FORALL(Cmds, commands(?MODULE),
	  begin
	      {ok, _Pid} = editor:new("test.txt"),
	      {History, State = {_FinalState, StateData}, Result} = run_commands(?MODULE, Cmds),
	      editor:close(),
	      timer:sleep(100),
	      ?WHENFAIL(io:format("History: ~w~nState: ~w\nResult: ~w~n",
				  [History, State, Result]),
			aggregate(eqc_statem:zip(state_names(History),
                                                 command_names(Cmds)),
				  conjunction([{execution_ok, Result =:= ok},
					       {doc_contents, expected_doc_contents(StateData, ?TEST_FILE)}
					      ])))
	  end).

% ============================================================================ %

insert_and_displace({K, Value}, [], Acc) ->
    lists:reverse([{K, Value} | Acc]);
insert_and_displace({K, Value}, [{K1, Value1} | T], Acc) when K1 < K ->
    insert_and_displace({K, Value}, T, [{K1, Value1}|Acc]);
insert_and_displace({K, Value}, [{K1, Value1} | T], Acc) when K1 == K ->
    lists:reverse([{K, Value} | Acc]) ++ displace(1, [{K1, Value1} | T], []);
insert_and_displace({K, Value}, [{K1, Value1} | T], Acc) when K1 > K ->
    lists:reverse([{K1, Value1}, {K, Value} | Acc]) ++ T.

displace(_, [], Acc) ->
    lists:reverse(Acc);
displace(N, [{K, Value} | T], Acc) ->
    displace(N, T, [{K+N, Value} | Acc]).

delete_and_displace(1, [_|T], Acc) ->
    lists:reverse(Acc) ++ displace(-1, T, []);
delete_and_displace(N, [H|T], Acc) ->
    delete_and_displace(N-1, T, [H|Acc]).

expected_doc_contents(StateData, ?TEST_FILE) ->
    case file:open(?TEST_FILE, [read]) of
	{error, enoent} ->
	    false;
	{ok, Fd} ->
	    check_lines(Fd, StateData)
    end.

check_lines(Fd, []) ->
    file:read_line(Fd) =:= eof;
check_lines(Fd, [{_, LineContents} | Rest]) ->
    ExpectedLine = LineContents ++ "\n",
    case file:read_line(Fd) of
	{ok, ExpectedLine} ->
	    check_lines(Fd, Rest);
	_Other ->
	    false
    end.

