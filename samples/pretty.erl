-module(pretty).

-compile(export_all).

print(Cmds,H) ->
    io:format("\n"),
    [pcall(Call,Res) ||
	{{set,_,Call},{_,Res}} <- eqc_statem:zip(Cmds,H++[{undefined,no_return}])].

pcall({call,_,Name,Args},R) ->
    io:format("~p(",[Name]),
    pargs(Args),
    io:format(") -> ~p~n",[R]).

pargs([]) ->
    ok;
pargs([X]) ->
    io:format("~p",[X]);
pargs([X|Args]) ->
    io:format("~p,",[X]),
    pargs(Args).
