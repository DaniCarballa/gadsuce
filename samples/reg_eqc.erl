-module(reg_eqc).

-include_lib("eqc/include/eqc.hrl").
-include_lib("eqc/include/eqc_statem.hrl").

-compile(export_all).

% Model of states

-define(names,[a,b,c,d]).

-record(state,{pids,	% list of spawned pids
	       regs}).	% list of registered names and pids

initial_state() ->
    #state{pids=[], regs=[]}.

% Command generator

command(S) ->
    oneof(
      [{call,erlang,register, [name(),elements(S#state.pids)]} ||
	  S#state.pids/=[]] ++
      [{call,erlang,unregister,[name()]},
       {call,?MODULE,spawn,[]},
       {call,erlang,whereis,[name()]}]).

name() -> 
    elements(?names).

% Preconditions

precondition(_S,{call,_,_,_}) ->
    true.

% State transitions

next_state(S,V,{call,?MODULE,spawn,[]}) ->
    S#state{pids=[V | S#state.pids]};
next_state(S,_V,{call,_,register,[Name,Pid]}) ->
    S#state{regs=[{Name,Pid} | S#state.regs]};
next_state(S,_V,{call,_,unregister,[Name]}) ->
    S#state{regs = lists:keydelete(Name,1,S#state.regs)};
next_state(S,_V,_) ->
    S.

% Postconditions

postcondition(_S,{call,_,_,_},_R) ->
    true.

% Correctness property

prop_registration() ->
    ?FORALL(Cmds,commands(?MODULE),
	    begin
		{H,_S,Res} = run_commands(?MODULE,Cmds),
                [catch erlang:unregister(N) || N<-?names],
		?WHENFAIL(begin
			      io:format("Reason: ~p\n",[Res]),
			      pretty:print(Cmds,H)
			  end,
			  Res==ok)
	    end).

% Operations for use in tests

spawn() -> 
    spawn(fun() -> receive after 30000 -> ok end end).

register(Name,Pid) ->
    catch erlang:register(Name,Pid).

unregister(Name) ->
    catch erlang:unregister(Name).

stop(Pid) ->
    exit(Pid,kill),
    timer:sleep(1).
