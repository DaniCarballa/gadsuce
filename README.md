# README #

## Generación automática de diagramas de secuencia UML a partir de contra-ejemplos en pruebas de unidad (GADSUCE) ##



### Idea principal ###

- Crear una herramienta que favorezca la visualización de contra-ejemplos en un formato estandarizado, esto es, que no dependa del formato generado por Quickcheck (u otras herramientas basadas en PBT). En concreto, se perseguirá la visualización en UML, integrando el sistema con la herramienta PlantUML, pero de modo que sea fácilmente sustituible por otras herramientas de generación de diagramas UML. De esta forma, se favorecerá la mantenibilidad y la extensibilidad.

### Versión ###

- Versión : 1.0
    * Implementación del módulo de lectura del contra-ejemplo (read).
    * Implementación del módulo principal (formato).
    * Implementación del módulo de escritura en el fichero (write_text).
  
- Versión : 2.0
    * Implementación de un supervisor (supervisor_format) para que monitorice el proceso de creación del diagrama (plantuml) y lo reinicie en caso de que se detecte algún problema.
    * Implementación de las pruebas de unidad.

- Versión : 3.0
      * Reordenación del repositorio.
      * Creación de un 'make' para automatizar la compilación, creación 'edoc' y ejecución de test.
      * Incluido un ejemplo de prueba, que se puede ejecutar desde el 'make'.
   
- Versión : 4.0
      * Algunas funciones no se pueden probar, por ello se ha instalado una herramienta para crear mocks en Erlang llamada 'Meck'.
      * En el archivo 'Makefile' se ha comentado la línea que prueba el módulo de pruebas 'Meck' para que no de ningún error ya que es una dependencia.
      * Se ha añadido otro ejemplo de prueba en la carpeta 'samples'.
   
    Para ejecutar el ejemplo hay que realizar los siguientes pasos :
    
      * `make sample` (esto compilará todo y abrirá un terminal erlang).
      * `eqc:module(reg_eqc).` o `eqc:module(editor_fsm).` (ejecuta las pruebas automáticas).
      * `formato:leer_contraejemplo()` o `formato:leer_contraejemplo("nombre")` (donde "nombre" puede ser cualquier cosa, pero entre comillas dobles).
      * Se crea el diagrama, lo puedes ver en la carpeta 'samples'.

### ¿Cómo configurar la herramienta? ###

- Se necesita Erlang/OTP 18 y java (para la ejecución del PlantUML)
- Descarga del ejecutable de PlantUML (PlantUML.jar) y meterlo en la carpeta 'lib' (ya incluido).
- Hay que ejecutar las pruebas con 'module' (tal y como se recomienda en el ejemplo), o de lo contrario no se garantiza mostrar todos los contra-ejemplos en el diagrama (en caso de ejecutarlas por separado):
`eqc:module(modulo).`

- Para crear el diagrama dentro de la consola de erlang hay dos posibilidades, sin parámetro o con parámetro. El primero, toma el nombre por defecto, 'textcomp', el segundo, el nombre que se le pase: 
`formato:leer_contraejemplo().`ó `formato:leer_contraejemplo("nombre")`

- Descargar del repositorio :
`git clone git@bitbucket.org:DaniCarballa/gadsuce.git`

### Contibución ###

- Daniel Carballa Besada
- Laura M. Castro

### Licencia ###

- GPL <http://www.gnu.org/licenses/gpl.html>

### Contacto ###

- Daniel Carballa Besada <danielcarballa@gmail.com>