%%% @author Laura M. Castro <lcastro@udc.es>
%%% @doc
%%%     Header file for configuration purposes.
%%% @end


-define(BASE_DIR, "./").
-define(LIBRARY_DIR, ?BASE_DIR ++ "lib/").

-define(PLANTUML_JAR, "plantuml.jar").

-define(OUTPUT_DIR, ?BASE_DIR ++ "samples/").
-define(OUTPUT_FILE, "textComp").
